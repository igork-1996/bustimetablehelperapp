package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/*
    Фрагмент для выбора остановки поиска
 */
public class ChoiceFindStation extends Fragment implements SearchView.OnQueryTextListener {
    View rootview;
    ArrayList<String> stationsString;
    ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_choice_find_station,container,false);
        setHasOptionsMenu(true);
        if(stationsString!=null) {
            adapter = new ArrayAdapter<>(rootview.getContext(), android.R.layout.simple_list_item_1, stationsString);
            ListView tmp = (ListView) rootview.findViewById(R.id.list_stations);
            tmp.setAdapter(adapter);
            TextView textView=(TextView) rootview.findViewById(R.id.idempty);
            textView.setText("Не найдено");
            tmp.setEmptyView(textView);
        }
        else
        {
            Toast.makeText(getActivity(),"Список пуст",Toast.LENGTH_SHORT).show();
        }
        return rootview;
    }
    boolean boolStart;
public void setStartOrStop(boolean start)
{
    boolStart=start;
}


    @Override
    public void onStart() {
        super.onStart();
        someEventListener = (onSomeEventListener) getActivity();
        final ListView tmpListView = (ListView)rootview.findViewById(R.id.list_stations);
        tmpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String strNameStation = adapter.getItem(i);
                if(boolStart)
                someEventListener.someEvent("1.2.1", strNameStation);
                else
                    someEventListener.someEvent("1.3.1", strNameStation);
            }
        });
    }

    //Метод устанавливает список остановок
    public void setStations(ArrayList<String> stations)
    {
        this.stationsString=stations;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        //super.onCreateOptionsMenu(menu, inflater);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        AutoCompleteTextView searchText = (AutoCompleteTextView) searchView.findViewById(R.id.search_src_text);
        searchText.setHintTextColor(getResources().getColor(R.color.white));
        searchText.setTextColor(getResources().getColor(R.color.white));
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return false;
    }

    public interface onSomeEventListener {
         void someEvent(String tagFragment,String stringSelected);
    }
    onSomeEventListener someEventListener;
}