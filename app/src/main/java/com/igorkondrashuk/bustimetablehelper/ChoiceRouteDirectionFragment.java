package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Фрагмент для выбора направления маршрута
 */
public class ChoiceRouteDirectionFragment extends Fragment{
    View rootview;
    ArrayList<String> routesDirectionsString;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.choice_route_direction_fragment,container,false);
        if(routesDirectionsString!=null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(rootview.getContext(), android.R.layout.simple_list_item_1, routesDirectionsString);
            ListView tmp = (ListView) rootview.findViewById(R.id.list_stations);
            tmp.setAdapter(adapter);
        }
        return rootview;
    }

    @Override
    public void onStart() {
        super.onStart();
        someEventListener = (onSomeEventListener) getActivity();
        final ListView tmpListView = (ListView)rootview.findViewById(R.id.list_stations);
        tmpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String strNumber =routesDirectionsString.get(i);

                someEventListener.someEvent("2.1",strNumber);
            }
        });
    }

    //Метод устанавливает список остановок
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }public void setDirections(ArrayList<String> directions)
    {
        this.routesDirectionsString=directions;
    }

    public interface onSomeEventListener {
        void someEvent(String tagFragment,String stringSelected);
    }
    onSomeEventListener someEventListener;
}
