package com.igorkondrashuk.bustimetablehelper;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Класс карточка для представления расписания маршрута
 */
public class CardlibRouteTimatable extends Card {
    protected TextView mTitle;
    protected TextView time;
    SpannableStringBuilder sb;
    String stationName;

    public CardlibRouteTimatable(Context context,String numberRoute,SpannableStringBuilder sb) {
        super(context, R.layout.cardlib_route_timetable);
        this.sb=sb;
        this.stationName=numberRoute;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);
        mTitle=(TextView)parent.findViewById(R.id.textNumberRoute);
        mTitle.setText(stationName);
        time=(TextView)parent.findViewById(R.id.textTime);
        time.setText(sb);
    }
}
