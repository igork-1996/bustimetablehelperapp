package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Фрагмент для выбора номера маршрута
 */
public class ChoiceRouteNumberFragment extends Fragment implements SearchView.OnQueryTextListener{
    View rootview;
    ArrayList<String> routesNumbersString;
    ArrayAdapter<String> adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        rootview = inflater.inflate(R.layout.choice_station_timetable,container,false);

        ArrayList<String> newArr= new ArrayList<>();
        if(routesNumbersString!=null) {
            for (int i = 0; i < routesNumbersString.size(); i++)
                newArr.add(routesNumbersString.get(i).replace("№", ""));
            adapter = new ArrayAdapter<>(rootview.getContext(), R.layout.choice_route_number_card, R.id.textNumberRoute, newArr);
            ListView tmp = (ListView) rootview.findViewById(R.id.list_stations);
            tmp.setAdapter(adapter);
            setListViewHeightBasedOnChildren(tmp);
            TextView textView=(TextView) rootview.findViewById(R.id.idempty);
            textView.setText("Не найдено");
            tmp.setEmptyView(textView);
        }
        return rootview;
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        someEventListener = (onSomeEventListener) getActivity();
        final ListView tmpListView = (ListView)rootview.findViewById(R.id.list_stations);
        tmpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String strNumber = adapter.getItem(i);

                someEventListener.someEvent("2", "№"+strNumber);
            }
        });
    }

    //Метод устанавливает список остановок
    public void setNumbers(ArrayList<String> routes)
    {
        this.routesNumbersString=routes;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        //super.onCreateOptionsMenu(menu, inflater);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        AutoCompleteTextView searchText = (AutoCompleteTextView) searchView.findViewById(R.id.search_src_text);
        searchText.setHintTextColor(getResources().getColor(R.color.white));
        searchText.setTextColor(getResources().getColor(R.color.white));
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return false;
    }

    public interface onSomeEventListener {
        void someEvent(String tagFragment,String stringSelected);
    }
    onSomeEventListener someEventListener;
}
