package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Фрагмент для отображения избранных маршрутов
 */
public class SelectedRouteFragment extends Fragment{
    View rootview;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.selected_station_fragment,container,false);
            final SharedPreferences sharedPref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
            String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper", "null");
            final Gson parser=new Gson();
            //Загрузка класса из SharedPreferences
            final SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
            }.getType());

            SelectedRouteListAdapter selectedRouteListAdapter=new SelectedRouteListAdapter(
                    getActivity(),
                    sharedPreferencesHelper.getFavoriteRouteNumbers(),
                    sharedPreferencesHelper.getFavoriteRouteDirections());
            ListView listView = (ListView) rootview.findViewById(R.id.list_stations);
            listView.setAdapter(selectedRouteListAdapter);
            TextView textView=(TextView) rootview.findViewById(R.id.idempty);
            textView.setText("Нет избранных маршрутов");
            listView.setEmptyView(textView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String strNumber = sharedPreferencesHelper.getFavoriteRouteNumbers().get(i) + "_" + sharedPreferencesHelper.getFavoriteRouteDirections().get(i);
                    someEventListener.someEvent("6.1", strNumber);
                }
            });
            someEventListener = (onSomeEventListener) getActivity();
        return rootview;
    }
    @Override
    public void onStart() {
        super.onStart();

    }

    public interface onSomeEventListener {
        void someEvent(String tagFragment, String stringSelected);
    }
    onSomeEventListener someEventListener;
}