package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.igorkondrashuk.bustimetablehelper.routemanager.Route;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteAnswer;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteManager;
import com.igorkondrashuk.bustimetablehelper.routemanager.Trip;

public class MapViewFragment extends Fragment {

    MapView mMapView;
    protected GoogleMap googleMap;
    RouteAnswer routeAnswer;
    RouteManager routeManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.location, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                for(Trip trip:routeAnswer.getArrayListTrip())
                {
                    Route route=routeManager.getRoute(
                            trip.getNumberRoute(),
                            trip.getDirectionRoute(),
                            trip.getDayRoute()
                    );
                    for(int i=route.getStations().indexOf(trip.getNameStartStation());
                        i<=route.getStations().indexOf(trip.getNameFinishStation());
                            i++)
                    {
                        int stationNumber=route.getStationsNumber().get(i);
                        //Номер 
                        if(stationNumber<0)
                            stationNumber*=-1;
                        String s[] = routeManager.getGpsHashMap().get(stationNumber).split(",");
                        double lat = Double.parseDouble(s[0]);
                        double longi = Double.parseDouble(s[1]);
                        LatLng sydney = new LatLng(longi,lat);
                        MarkerOptions marker=new MarkerOptions().position(sydney).title(Integer.toString(i)).snippet(route.getStations().get(i));
                        googleMap.addMarker(marker);
                    }
                }
                LatLng sydney = new LatLng(52.0880064,23.6875285);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(13).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void setRouteAnswer(RouteAnswer routeAnswer) {
        this.routeAnswer = routeAnswer;
    }

    public void setRouteManager(RouteManager routeManager) {
        this.routeManager = routeManager;
    }
}