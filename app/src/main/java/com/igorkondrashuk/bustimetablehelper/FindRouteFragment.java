package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.igorkondrashuk.bustimetablehelper.routemanager.FindAnswer;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

/**
 * Фрагмент для отображения ответа на запрос поиска маршрутов
 */
public class FindRouteFragment extends Fragment {
    FindAnswer findAnswer;
    View rootview;
    CardListView cardView;
    String startStationName;
    String finishSrarionName;
    public void setFindInfo(String name,String number)
    {
        this.startStationName =name;
        this.finishSrarionName =number;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.find_answer_fragment,container,false);
        cardView = (CardListView) rootview.findViewById(R.id.cardlib_find_answer);
        if(findAnswer !=null) {
            ArrayList<Card> cards= new ArrayList<>();
            CardlibDayComment timeCard=new CardlibDayComment(getActivity(), findAnswer.getDayComment());
            if (findAnswer.getAnswers().size() != 0) {
                cards.add(timeCard);
                for (int i = 0; i < findAnswer.getAnswers().size() && i < 20; i++) {
                    CardlibRouteAnswer card = new CardlibRouteAnswer(
                            getActivity(),
                            findAnswer.getAnswers().get(i));
                    card.setSwipeable(false);
                    card.init();
                    cards.add(card);
                }
                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(getActivity(), cards);
                cardView.setAdapter(mCardArrayAdapter);
            }
}
        final FloatingActionButton fab = (FloatingActionButton) rootview.findViewById(R.id.fab);
        fab.setColorNormalResId(R.color.primary_dark);
        fab.setShadow(false);
        final SharedPreferences sharedPref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper", "null");
        final Gson parser=new Gson();
        //Загрузка класса из SharedPreferences
        final SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
        }.getType());
        if(sharedPreferencesHelper.isFavoriteFindStations(startStationName,finishSrarionName))
        {
            Drawable tempImage = getResources().getDrawable(R.drawable.icon_full_star);
            fab.setImageDrawable(tempImage);
        }
        else
        {
            Drawable tempImage = getResources().getDrawable(R.drawable.ic_border_star);
            fab.setImageDrawable(tempImage);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPreferencesHelper.isFavoriteFindStations(startStationName,finishSrarionName))
                {
                    Drawable tempImage = getResources().getDrawable(R.drawable.ic_border_star);
                    fab.setImageDrawable(tempImage);
                    sharedPreferencesHelper.deleteFavoriteFindStations(startStationName,finishSrarionName);
                }
                else
                {
                    Drawable tempImage = getResources().getDrawable(R.drawable.icon_full_star);
                    fab.setImageDrawable(tempImage);
                    sharedPreferencesHelper.addFavoriteFindStations(startStationName,finishSrarionName);
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                editor.apply();
            }

        });
        TextView textView=(TextView) rootview.findViewById(R.id.idempty);
        if(findAnswer.getCountOneRoute()!=0||
                findAnswer.getCountTwoRoute()!=0)
            textView.setText("К сожалению, на сегодня рейсов нет, попробуйте выбрать другое время.");
        else
            textView.setText("К сожалению, не найдено вариантов для поездки, попробуйте выбрать другие остановки.");
        cardView.setEmptyView(textView);
        fab.attachToListView(cardView);
        return rootview;
    }
    public void setFindAnswer(FindAnswer findAnswer)
    {
        this.findAnswer = findAnswer;
    }
}
