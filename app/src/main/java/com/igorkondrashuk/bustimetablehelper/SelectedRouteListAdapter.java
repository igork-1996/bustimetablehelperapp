package com.igorkondrashuk.bustimetablehelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Адаптер для отображения избранных маршрутов в списке
 */

public class SelectedRouteListAdapter extends BaseAdapter {
    ArrayList<String> routeNumber;
    ArrayList<String> routeDirection;
    Activity context;
    final SharedPreferences sharedPref;
    public SelectedRouteListAdapter(Activity context, ArrayList<String> firstStation, ArrayList<String> routeDirection) {
        this.routeNumber = firstStation;
        this.routeDirection = routeDirection;
        this.context=context;
        sharedPref= context.getSharedPreferences("pref", Context.MODE_PRIVATE);
    }
    @Override
    public int getCount() {
        return routeNumber.size();
    }

    @Override
    public Object getItem(int position) {
        return routeNumber.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if(convertView==null)
            convertView=context.getLayoutInflater().inflate(R.layout.selected_route, parent, false);

        TextView textViewStart=(TextView)convertView.findViewById(R.id.textViewStarRoute);
        textViewStart.setText(routeNumber.get(position).replace("№", "") + " " + routeDirection.get(position));
        ImageButton imageButton =(ImageButton)convertView.findViewById(R.id.imageButtonStarRoute);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper", "null");
                Gson parser=new Gson();
                //Загрузка класса из SharedPreferences
                SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
                }.getType());
                if(sharedPreferencesHelper.isFavoriteRoute(routeNumber.get(position),routeDirection.get(position)))
                {
                    v.setBackgroundDrawable(parent.getResources().getDrawable(R.drawable.ic_border_star_gold_50));
                    sharedPreferencesHelper.deleteFavoriteRoute(routeNumber.get(position),routeDirection.get(position));
                }
                else
                {
                    v.setBackgroundDrawable(parent.getResources().getDrawable(R.drawable.ic_full_star_gold_50));
                    sharedPreferencesHelper.addFavoriteRoute(routeNumber.get(position),routeDirection.get(position));
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                editor.apply();
            }
        });
        return convertView;
    }
}