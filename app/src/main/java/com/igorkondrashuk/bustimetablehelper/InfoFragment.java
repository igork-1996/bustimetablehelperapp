package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Фрагмент для отображения информации о приложении
 */
public class InfoFragment extends Fragment {
    View rootview;
    //Данные о расписании
    String versionTimetable;
    String dataTimetable;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.info_fragment,container,false);
        TextView tempText=(TextView)rootview.findViewById(R.id.textViewInfo);
        tempText.setText("Расписание от " + dataTimetable + "\nВерсия базы данных " + versionTimetable);
        return rootview;
    }
    //Метод устанавливает информацию о расписании
    public void setInfo(String versionTimetable,String dataTimetable)
    {
        this.versionTimetable=versionTimetable;
        this.dataTimetable=dataTimetable;
    }
}
