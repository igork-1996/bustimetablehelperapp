package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Класс для выбора названия остановки
 */
public class ChoiceStationFragment extends Fragment implements SearchView.OnQueryTextListener {
    View rootview;
    ArrayList<String> stationsString;
    ArrayAdapter<String> adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.choice_station_timetable,container,false);
        setHasOptionsMenu(true);
        if(stationsString!=null) {
            //С помошью стандартного адаптера создаем список названий остановок для выбора
            adapter = new ArrayAdapter<>(rootview.getContext(), R.layout.choice_station_card, R.id.textStationChoice, stationsString);
            ListView tmpListView = (ListView) rootview.findViewById(R.id.list_stations);
            tmpListView.setAdapter(adapter);
            //TextView для отображения в случае пустого списка
            TextView textView=(TextView) rootview.findViewById(R.id.idempty);
            textView.setText("Не найдено");
            tmpListView.setEmptyView(textView);
            //Установка слушателя для нажатия на элемент списка
            someEventListener = (onSomeEventListener) getActivity();
            tmpListView = (ListView)rootview.findViewById(R.id.list_stations);
            tmpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String strNameStation = adapter.getItem(i);
                    someEventListener.someEvent("1", strNameStation);
                }
            });
        }
        return rootview;
    }
    //Метод устанавливает список остановок
    public void setStations(ArrayList<String> stations)
    {
        this.stationsString=stations;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        //Установка поиска в toolbar
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        AutoCompleteTextView searchText = (AutoCompleteTextView) searchView.findViewById(R.id.search_src_text);
        searchText.setHintTextColor(getResources().getColor(R.color.white));
        searchText.setTextColor(getResources().getColor(R.color.white));
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return false;
    }

    public interface onSomeEventListener {
        void someEvent(String tagFragment,String stringSelected);
    }
    onSomeEventListener someEventListener;
}
