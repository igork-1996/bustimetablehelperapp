package com.igorkondrashuk.bustimetablehelper.routemanager;

import java.util.ArrayList;
/*
	Класс для представления данных о расписании маршрута
 */
public class RouteTimetable
{
	private String number;
	private String direction;
	private String dayComment;
	private ArrayList<ArrayList<TimetableStationOnRoute>> stations;
	RouteTimetable(String number, String direction)
	{
		this.number = number;
		this.direction = direction;
	}
	public void setStations(ArrayList<ArrayList<TimetableStationOnRoute>> stations) {
		this.stations = stations;
	}

	public void setDayComment(String dayComment) {
		this.dayComment = dayComment;
	}
	public ArrayList<ArrayList<TimetableStationOnRoute>> getStations() {
		return stations;
	}

	public String getNumber() {
		return number;
	}

	public String getDirection() {
		return direction;
	}

	public String getDayComment() {
		return dayComment;
	}
}
