package com.igorkondrashuk.bustimetablehelper.routemanager;

import java.util.ArrayList;
/*
	Класс для представления расписания остановки
 */
public class StationTimetable
{
	private String stationName;
	private int stationNumber;
	private String dayComment;
	private ArrayList<ArrayList<TimetableRouteOnStation>> routes;

	StationTimetable(String stationName, int stationNumber)
	{
		this.stationName = stationName;
		this.setStationNumber(stationNumber);
	}
	void addRoutes(ArrayList<ArrayList<TimetableRouteOnStation>> routes)
	{
		this.routes = routes;
	}

	public String getStationName() {
		return stationName;
	}

	public int getStationNumber() {
		return stationNumber;
	}

	public String getDayComment() {
			return dayComment;
	}

	public ArrayList<ArrayList<TimetableRouteOnStation>> getRoutes() {
		return routes;
	}

	public void setStationNumber(int stationNumber) {
		this.stationNumber = stationNumber;
	}

	public void setDayComment(String dayComment) {
		this.dayComment = dayComment;
	}
}