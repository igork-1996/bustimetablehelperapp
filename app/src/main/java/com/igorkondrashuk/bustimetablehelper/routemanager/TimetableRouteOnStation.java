package com.igorkondrashuk.bustimetablehelper.routemanager;

import java.util.ArrayList;

/*
	Класс для представления маршрута в расписании остановки
 */

public class TimetableRouteOnStation
{
	private String number;
	private String direction;
	private String day;
	private ArrayList<String> time;
	private ArrayList<Integer> colorTime;


	TimetableRouteOnStation(String number, String direction, String day, ArrayList<String> timeTemp)
	{
		ArrayList<String> timeAdd = new ArrayList<>();
		for(String aTimeTemp : timeTemp)
			if(!aTimeTemp.equals(""))
				timeAdd.add(aTimeTemp);
		this.time = timeAdd;
		this.number = number;
		this.day = day;
		this.direction = direction;
	}

	public String getNumber() {
		return number;
	}

	public String getDirection() {
		return direction;
	}

	public String getDay() {
		return day;
	}

	public ArrayList<String> getTime() {
		return time;
	}

	public ArrayList<Integer> getColorTime() {
		return colorTime;
	}

	public void setColorTime(ArrayList<Integer> colorTime) {
		this.colorTime = colorTime;
	}
}