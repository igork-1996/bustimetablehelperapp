package com.igorkondrashuk.bustimetablehelper.routemanager;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Класс для представления маршрута из расписания
 */

public class Route implements Serializable
{

    private String number;
    private String day;
    private String direction;
    private ArrayList<String> stations;
    private ArrayList<Integer> stationsNumber;
    private ArrayList<ArrayList<String>> stationsTime;
    public Route(String number, String direction, String day)
    {
        this.number = number;
        this.day = day;
        this.direction = direction;
        stations = new ArrayList<>();
        stationsNumber = new ArrayList<>();
        stationsTime = new ArrayList<>();
    }
    public String getNumber() {
        return number;
    }

    public String getDay() {
        return day;
    }

    public String getDirection() {
        return direction;
    }

    public ArrayList<String> getStations() {
        return stations;
    }

    public ArrayList<Integer> getStationsNumber() {
        return stationsNumber;
    }

    public ArrayList<ArrayList<String>> getStationsTime() {
        return stationsTime;
    }

}
