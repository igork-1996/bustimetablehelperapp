package com.igorkondrashuk.bustimetablehelper.routemanager;

/**
 * Класс для хранения данных о поездке на одном транспортном марщруте
 */
public class Trip
{
    private String nameStartStation;
    private Integer numberStartStation;
    private String nameFinishStation;
    private Integer numberFinishStation;
    private String numberRoute;
    private String directionRoute;
    private String dayRoute;
    private String timeStart;
    private String timeStop;
    Trip(String nameStartStation,
                Integer numberStartStation,
                String nameFinishStation,
                Integer numberFinishStation,
                String numberRoute,
                String directionRoute,
                String dayRoute,
                String timeStart,
                String timeStop
                ) {
        this.setNameStartStation(nameStartStation);
        this.setNumberStartStation(numberStartStation);
        this.setNameFinishStation(nameFinishStation);
        this.setNumberFinishStation(numberFinishStation);
        this.setNumberRoute(numberRoute);
        this.setDirectionRoute(directionRoute);
        this.setDayRoute(dayRoute);
        this.setTimeStart(timeStart);
        this.setTimeStop(timeStop);
    }

    public String getNameStartStation() {
        return nameStartStation;
    }

    public void setNameStartStation(String nameStartStation) {
        this.nameStartStation = nameStartStation;
    }

    public Integer getNumberStartStation() {
        return numberStartStation;
    }

    public void setNumberStartStation(Integer numberStartStation) {
        this.numberStartStation = numberStartStation;
    }

    public String getNameFinishStation() {
        return nameFinishStation;
    }

    public void setNameFinishStation(String nameFinishStation) {
        this.nameFinishStation = nameFinishStation;
    }

    public Integer getNumberFinishStation() {
        return numberFinishStation;
    }

    public void setNumberFinishStation(Integer numberFinishStation) {
        this.numberFinishStation = numberFinishStation;
    }

    public String getNumberRoute() {
        return numberRoute;
    }

    public void setNumberRoute(String numberRoute) {
        this.numberRoute = numberRoute;
    }

    public String getDirectionRoute() {
        return directionRoute;
    }

    public void setDirectionRoute(String directionRoute) {
        this.directionRoute = directionRoute;
    }

    public String getDayRoute() {
        return dayRoute;
    }

    public void setDayRoute(String dayRoute) {
        this.dayRoute = dayRoute;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeStop() {
        return timeStop;
    }

    public void setTimeStop(String timeStop) {
        this.timeStop = timeStop;
    }
}
