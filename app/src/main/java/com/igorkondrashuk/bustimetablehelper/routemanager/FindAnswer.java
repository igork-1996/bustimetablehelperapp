package com.igorkondrashuk.bustimetablehelper.routemanager;

import java.util.ArrayList;

/**
 * Класс для хранения ответа на запрос
 */
public class FindAnswer {
    private String dayComment;
    private String startStation;
    private String finishStation;
    private int countOneRoute;
    private int countTwoRoute;
    private ArrayList<RouteAnswer> answers;

    public FindAnswer(String dayComment, String startStation, String finishStation, int countOneRoute, int countTwoRoute, ArrayList<RouteAnswer> answers) {
        this.dayComment = dayComment;
        this.startStation = startStation;
        this.finishStation = finishStation;
        this.countOneRoute = countOneRoute;
        this.countTwoRoute = countTwoRoute;
        this.answers = answers;
    }

    public String getStartStation()
    {
            return startStation;
    }
    public String getFinishStation()
    {
            return finishStation;
    }
    public String getDayComment()
    {
        return dayComment;
    }
    public ArrayList<RouteAnswer> getAnswers() {
        return answers;
    }
    public int getCountOneRoute() {
        return countOneRoute;
    }
    public int getCountTwoRoute() {
        return countTwoRoute;
    }
}
