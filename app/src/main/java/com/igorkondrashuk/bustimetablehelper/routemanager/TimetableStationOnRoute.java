package com.igorkondrashuk.bustimetablehelper.routemanager;

import java.util.ArrayList;

/*
    Класс для представления остановки в расписании маршрута
 */
public class TimetableStationOnRoute
{
	private String stationName;
	private int stationNumber;
	private String dayRoute;
	private ArrayList<String> timesRoute;

    public void setColorsTimesRoute(ArrayList<Integer> colorsTimesRoute) {
        this.colorsTimesRoute = colorsTimesRoute;
    }

    private ArrayList<Integer> colorsTimesRoute;
	public TimetableStationOnRoute(String stationName,
                                   int stationNumber,
                                   String dayRoute, ArrayList<String> timesRoute) {
		this.stationName = stationName;
		this.stationNumber = stationNumber;
		this.dayRoute = dayRoute;
		this.timesRoute = timesRoute;
	}

	public String getStationName() {
		return stationName;
	}

	public int getStationNumber() {
		return stationNumber;
	}

	public String getDayRoute() {
		return dayRoute;
	}

	public ArrayList<String> getTimesRoute() {
		return timesRoute;
	}

	public ArrayList<Integer> getColorsTimesRoute() {
		return colorsTimesRoute;
	}


}