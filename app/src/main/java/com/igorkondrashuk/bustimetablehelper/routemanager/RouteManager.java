package com.igorkondrashuk.bustimetablehelper.routemanager;

import java.util.*;
/*
    Класс для работы с расписанием
 */
public class RouteManager
{
    //HashMap хранит координаты для соответствующего номера остановки
    private HashMap<Integer,String> gpsHashMap;
    //HashMap хранит название соответствующего номера остановки
    private HashMap<Integer,String> hashMapNumberNameStation;
    //Массив хранит список всех номеров маршрутов
    private ArrayList<String> allNumberRoutes;
    //Массив хранит список всех номеров остановок
    private ArrayList<Integer> allNumberStations;
    //Список всех маршрутов
    private ArrayList<Route> allRoutes;
    //Граф ханит связи между номерами остановок
    //узел-номер остановки
    //ребро-индекс маршрута в списке allRoutes
    private ArrayList<ArrayList<ArrayList<Integer>>> graph;
    //Класс для работы со временем
    private TimeManager timeManager;

    public RouteManager(
            ArrayList<Route> allRoutes,
            ArrayList<Integer> allNumberStations,
            HashMap<Integer,String> gpsHashMap,
            HashMap<Integer,String> hashMapNumberNameStation,
            ArrayList<ArrayList<ArrayList<Integer>>> graph,
            TimeManager timeManager)
    {
        this.allRoutes = allRoutes;
        this.allNumberStations=allNumberStations;
        this.hashMapNumberNameStation=hashMapNumberNameStation;
        this.gpsHashMap=gpsHashMap;
        this.graph=graph;
        this.timeManager=timeManager;
        allNumberRoutes=new ArrayList<>();
        for(Route route:allRoutes)
        {
            if(!allNumberRoutes.contains(route.getNumber()))
                allNumberRoutes.add(route.getNumber());
        }
        Collections.sort(allNumberRoutes, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                lhs = lhs.replaceAll("\\D", "");
                rhs = rhs.replaceAll("\\D", "");
                Integer left = Integer.parseInt(lhs);
                Integer right = Integer.parseInt(rhs);
                return left.compareTo(right);
            }
        });
    }

    //Метод, который возвращает направления выбранного маршрута
    public ArrayList<String> getListRouteDirections(String routeNumber)
    {
        ArrayList<String> routeDirections=new ArrayList<>();
        for(Route route : allRoutes)
        if(route.getNumber().equals(routeNumber)&&
                !routeDirections.contains(route.getDirection()))
            routeDirections.add(route.getDirection());
        return routeDirections;
    }
    public FindAnswer findRoutes(String stationNameStart, String stationNameFinish, Integer year, Integer month, Integer day, Integer hour, Integer minute)
    {
        //Определяем ближайшие остановки к заданным
        ArrayList<String> stationsA=new ArrayList<>();
        ArrayList<String> stationsB=new ArrayList<>();
        ArrayList<String> allStationNames= getAllStationNames();
        //Определяем ближайшие остановки к стартовой
        stationsA.add(stationNameStart);
        ArrayList<Integer> arrayListStartStationNumbers=getArrayListStationNumbers(stationNameStart);
        for (int finishStationNumber : arrayListStartStationNumbers) {
            for (String stationName : allStationNames) {
                ArrayList<Integer> tempArrayListStationNumbers = getArrayListStationNumbers(stationName);
                for (int tempArrayListStationNumber : tempArrayListStationNumbers) {
                    String strGpsA = gpsHashMap.get(finishStationNumber);
                    String strGpsB = gpsHashMap.get(tempArrayListStationNumber);
                    if (strGpsA != null && strGpsB != null) {
                        String[] splitStrA = strGpsA.split(",");
                        String[] splitStrB = strGpsB.split(",");
                        double distance = distance(
                                Double.parseDouble(splitStrA[0]),
                                Double.parseDouble(splitStrB[0]),
                                Double.parseDouble(splitStrA[1]),
                                Double.parseDouble(splitStrB[1]),
                                Double.parseDouble(splitStrA[2]),
                                Double.parseDouble(splitStrB[2]));
                        if (distance <= 350 && !stationsA.contains(stationName))
                            stationsA.add(stationName);
                    }
                }
            }
        }
        //Определяем ближайшие остановки к конечной
        stationsB.add(stationNameFinish);
        ArrayList<Integer> arrayListFinishStationNumbers=getArrayListStationNumbers(stationNameFinish);
        for (int arrayListStartStationNumber : arrayListFinishStationNumbers) {
            for (String stationName : allStationNames) {
                ArrayList<Integer> tempArrayListStationNumbers = getArrayListStationNumbers(stationName);
                for (int tempArrayListStationNumber : tempArrayListStationNumbers) {
                    String strGpsA = gpsHashMap.get(arrayListStartStationNumber);
                    String strGpsB = gpsHashMap.get(tempArrayListStationNumber);
                    if (strGpsA != null && strGpsB != null) {
                        String[] splitStrA = strGpsA.split(",");
                        String[] splitStrB = strGpsB.split(",");
                        double distance = distance(
                                Double.parseDouble(splitStrA[0]),
                                Double.parseDouble(splitStrB[0]),
                                Double.parseDouble(splitStrA[1]),
                                Double.parseDouble(splitStrB[1]),
                                Double.parseDouble(splitStrA[2]),
                                Double.parseDouble(splitStrB[2]));
                        if (distance <= 350 && !stationsB.contains(stationName))
                            stationsB.add(stationName);
                    }
                }
            }
        }
        ArrayList<Integer> stationsNumbersA=new ArrayList<>();
        for(String stationNameA:stationsA)
                stationsNumbersA.addAll(getArrayListStationNumbers(stationNameA));

        ArrayList<Integer> stationsNumbersB=new ArrayList<>();
        for(String stationNameB:stationsB)
            stationsNumbersB.addAll(getArrayListStationNumbers(stationNameB));
        //Массив с ответами
        ArrayList<ArrayList<Trip>> arrayList=new ArrayList<>();
        //Ищем сначала без пересадок
        ArrayList<ArrayList<Trip>> oneRoute=findTrip(stationsNumbersA,stationsNumbersB,year,month,day);
        //К-во разных найденых маршрутов
        int countOneRoute=oneRoute.size();
        //Находим время для найденных вариантов
        oneRoute=findTimeForArrListRoute(oneRoute,hour,minute);
        //Добавляем в конечный ответ
        arrayList.addAll(oneRoute);
        int countTwoRoute=0;
        //Затем с одной пересадкой
        if(countOneRoute==0) {
            ArrayList<ArrayList<Trip>> twoRoute = findTripTwo(stationsNumbersA,stationsNumbersB,year,month,day);
            countTwoRoute=twoRoute.size();
            //Находим время для найденных вариантов
            twoRoute=findTimeForArrListRoute(twoRoute,hour,minute);
            arrayList.addAll(twoRoute);
        }
        ArrayList<RouteAnswer> arrayRouteAnswer=new ArrayList<>();
        for(ArrayList<Trip> route:arrayList)
        {
            int tripTime=timeManager.timeComp(route.get(0).getTimeStart(), route.get(route.size() - 1).getTimeStop());
            int allTime=timeManager.timeComp(timeToString(hour,minute), route.get(route.size() - 1).getTimeStop());
            arrayRouteAnswer.add(new RouteAnswer(route,tripTime,allTime));
        }
        Collections.sort(arrayRouteAnswer, new Comparator<RouteAnswer>() {
            @Override
            public int compare(RouteAnswer o1, RouteAnswer o2) {
                return o1.getAllTime()-o2.getAllTime();
            }
        });
        return new FindAnswer(timeManager.getCommentForDay(year,month,day),stationNameStart,stationNameFinish,countOneRoute,countTwoRoute,arrayRouteAnswer);
    }
    private ArrayList<ArrayList<Trip>> findTrip(ArrayList<Integer> stationsA, ArrayList<Integer> stationsB, int year, int month, int day)
    {
        /**
         * Выбор всех возможных вариантов поездки
         * Получаем массив с вариантами для поездки, сгрупированными по одинаковым маршрутам, но разным остановкам
         */
        //Массив со всевозможными предполагаемыми маршрутами
        ArrayList<ArrayList<ArrayList<Trip>>> arrayListRoutes=new ArrayList<>();
        //Hash варианта поездки
        ArrayList<String> hashRoutes=new ArrayList<>();
        for (int aStationsA : stationsA)
            for (int aStationsB : stationsB) {
                for (int k = 0; k < graph.get(aStationsA).get(aStationsB).size(); k++) {
                    if(timeManager.isRightDay(
                            allRoutes.get(graph.get(aStationsA).get(aStationsB).get(k)).getDay(),
                            year,
                            month,
                            day)) {
                        ArrayList<Trip> tempArrayList = new ArrayList<>();
                        tempArrayList.add(new Trip(
                                hashMapNumberNameStation.get(aStationsA),
                                aStationsA,
                                hashMapNumberNameStation.get(aStationsB),
                                aStationsB,
                                allRoutes.get(graph.get(aStationsA).get(aStationsB).get(k)).getNumber(),
                                allRoutes.get(graph.get(aStationsA).get(aStationsB).get(k)).getDirection(),
                                allRoutes.get(graph.get(aStationsA).get(aStationsB).get(k)).getDay(),
                                "-",
                                "-"));
                        String hashString =
                                allRoutes.get(graph.get(aStationsA).get(aStationsB).get(k)).getNumber() +
                                        allRoutes.get(graph.get(aStationsA).get(aStationsB).get(k)).getDirection() +
                                        allRoutes.get(graph.get(aStationsA).get(aStationsB).get(k)).getDay();
                        int p = hashRoutes.indexOf(hashString);
                        if (p == -1) {
                            hashRoutes.add(hashString);
                            ArrayList<ArrayList<Trip>> route = new ArrayList<>();
                            route.add(tempArrayList);
                            arrayListRoutes.add(route);
                        } else {
                            arrayListRoutes.get(p).add(tempArrayList);
                        }
                    }
                }
            }
        /**
         * Оставляем из каждой группы только один наиболее подходящий маршрут
         */
        for (ArrayList<ArrayList<Trip>> anArrayListRoute : arrayListRoutes) {
            /**
             * Отсеиваем маршруты по стартовой остановке
             * Находим ближайшую остановку к необходимой стартовой
             */
            ArrayList<String> stations = new ArrayList<>();
            ArrayList<Integer> stationsNumber = new ArrayList<>();
            for (ArrayList<Trip> route : anArrayListRoute) {
                //получаем список осттановок
                stations.add(route.get(0).getNameStartStation());
                //определяем насколько подходит остановка
                stationsNumber.add(stationsA.indexOf(route.get(0).getNumberStartStation()));
            }
            if (stations.size() != 0) {
                //Находим наиболее подходящую начальную остановку
                int minStation = stationsNumber.get(0);
                String minStationName = stations.get(0);
                for (int j = 1; j < stationsNumber.size(); j++) {
                    if (minStation > stationsNumber.get(j)) {
                        minStation = stationsNumber.get(j);
                        minStationName = stations.get(j);
                    }
                }
                //Удаляем маршруты, которые не выходят из наиболее подходящей остановки
                for (int j = anArrayListRoute.size() - 1; j >= 0; j--) {
                    if (!anArrayListRoute.get(j).get(0).getNameStartStation().equals(minStationName))
                        anArrayListRoute.remove(j);
                }
            }
            /**
             * Отсеиваем маршруты по конечной остановке
             * Находим ближайшую остановку к необходимой конечной
             */
            stations = new ArrayList<>();
            stationsNumber = new ArrayList<>();
            for (ArrayList<Trip> route : anArrayListRoute) {
                //получаем список остановок
                stations.add(route.get(0).getNameFinishStation());
                //определяем насколько подходит остановка
                stationsNumber.add(stationsB.indexOf(route.get(0).getNumberFinishStation()));
            }
            if (stations.size() != 0) {
                //Находим наиболее подходящую начальную остановку
                int minStation = stationsNumber.get(0);
                String minStationName = stations.get(0);
                for (int j = 1; j < stationsNumber.size(); j++) {
                    if (minStation > stationsNumber.get(j)) {
                        minStation = stationsNumber.get(j);
                        minStationName = stations.get(j);
                    }
                }
                //Удаляем маршруты, которые не выходят из наиболее подходящей остановки
                for (int j = anArrayListRoute.size() - 1; j >= 0; j--) {
                    if (!anArrayListRoute.get(j).get(0).getNameFinishStation().equals(minStationName))
                        anArrayListRoute.remove(j);
                }
            }
        }
        ArrayList<ArrayList<Trip>> finalArrayListRoutes=new ArrayList<>();
        for (ArrayList<ArrayList<Trip>> arrayListRoute : arrayListRoutes) {
            finalArrayListRoutes.add(arrayListRoute.get(0));
        }
        return finalArrayListRoutes;
    }
    private ArrayList<ArrayList<Trip>> findTripTwo(ArrayList<Integer> stationsA, ArrayList<Integer> stationsC, int year, int month, int day)
    {
        /**
         * Выбор всех возможных вариантов поездки
         * Получаем массив с вариантами для поездки, сгрупированными по одинаковым маршрутам, но разным остановкам
         */
        //Массив со всевозможными предполагаемыми маршрутами
        ArrayList<ArrayList<ArrayList<Trip>>> arrayListRoutes=new ArrayList<>();
        //Hash варианта поездки
        ArrayList<String> hashRoutes=new ArrayList<>();
        ArrayList<Trip> routeFromAToAB=new ArrayList<>();
        for(int aStationA:stationsA)
        {
            for(int i=0;i<graph.get(aStationA).size();i++)
                    for(int numberRouteInArray : graph.get(aStationA).get(i))
                    {
                        if(timeManager.isRightDay(allRoutes.get(numberRouteInArray).getDay(),year,month,day))
                        routeFromAToAB.add(new Trip(
                                hashMapNumberNameStation.get(aStationA),
                                aStationA,
                                hashMapNumberNameStation.get(i),
                                i,
                                allRoutes.get(numberRouteInArray).getNumber(),
                                allRoutes.get(numberRouteInArray).getDirection(),
                                allRoutes.get(numberRouteInArray).getDay(),
                                "-",
                                "-"
                        ));
                    }
        }
        ArrayList<Trip> routeFromBCToC=new ArrayList<>();
        for(int aStationC:stationsC)
        {
            for(int i=0;i<graph.get(aStationC).size();i++)
                    for(int numberRouteInArray : graph.get(i).get(aStationC))
                    {
                        if(timeManager.isRightDay(allRoutes.get(numberRouteInArray).getDay(),year,month,day))
                        routeFromBCToC.add(new Trip(
                                hashMapNumberNameStation.get(i),
                                i,
                                hashMapNumberNameStation.get(aStationC),
                                aStationC,
                                allRoutes.get(numberRouteInArray).getNumber(),
                                allRoutes.get(numberRouteInArray).getDirection(),
                                allRoutes.get(numberRouteInArray).getDay(),
                                "-",
                                "-"
                        ));
                    }
        }
        for(Trip aTripFromAToAB:routeFromAToAB)
                for(Trip aTripFromBCToC:routeFromBCToC)
                {
                    if(isStationsNode(aTripFromAToAB.getNumberFinishStation(), aTripFromBCToC.getNumberStartStation()))
                    {
                        String hashRoute=
                                aTripFromAToAB.getNumberRoute() +
                                        aTripFromAToAB.getDirectionRoute() +
                                        aTripFromAToAB.getDayRoute() +
                                        aTripFromBCToC.getNumberRoute() +
                                        aTripFromBCToC.getDirectionRoute() +
                                        aTripFromBCToC.getDayRoute();
                        if(hashRoutes.contains(hashRoute))
                        {
                            ArrayList<Trip> tempArrayRoutes=new ArrayList<>();
                            tempArrayRoutes.add(aTripFromAToAB);
                            tempArrayRoutes.add(aTripFromBCToC);
                            arrayListRoutes.get(hashRoutes.indexOf(hashRoute)).add(tempArrayRoutes);
                        }
                        else
                        {
                            hashRoutes.add(hashRoute);
                            ArrayList<Trip> tempArrayRoutes=new ArrayList<>();
                            tempArrayRoutes.add(aTripFromAToAB);
                            tempArrayRoutes.add(aTripFromBCToC);
                            ArrayList<ArrayList<Trip>> tempArray= new ArrayList<>();
                            tempArray.add(tempArrayRoutes);
                            arrayListRoutes.add(tempArray);

                        }
                    }
                }
/**
 * Оставляем из каждой группы только один наиболее подходящий маршрут
 */
        for (ArrayList<ArrayList<Trip>> anArrayListRoute : arrayListRoutes) {
            /**
             * Отсеиваем маршруты по стартовой остановке
             * Находим ближайшую остановку к необходимой стартовой
             */
            ArrayList<String> stations = new ArrayList<>();
            ArrayList<Integer> stationsNumber = new ArrayList<>();
            for (ArrayList<Trip> route : anArrayListRoute) {
                //получаем список осттановок
                stations.add(route.get(0).getNameStartStation());
                //определяем насколько подходит остановка
                stationsNumber.add(stationsA.indexOf(route.get(0).getNumberStartStation()));
            }
            if (stations.size() != 0) {
                //Находим наиболее подходящую начальную остановку
                int minStation = stationsNumber.get(0);
                String minStationName = stations.get(0);
                for (int j = 1; j < stationsNumber.size(); j++) {
                    if (minStation > stationsNumber.get(j)) {
                        minStation = stationsNumber.get(j);
                        minStationName = stations.get(j);
                    }
                }
                //Удаляем маршруты, которые не выходят из наиболее подходящей остановки
                for (int j = anArrayListRoute.size() - 1; j >= 0; j--) {
                    if (!anArrayListRoute.get(j).get(0).getNameStartStation().equals(minStationName))
                        anArrayListRoute.remove(j);
                }
            }
            /**
             * Отсеиваем маршруты по начальной остановке второго маршрута
             * Находим последнюю по ходу движения автобуса остановку
             */
            stations = new ArrayList<>();
            stationsNumber = new ArrayList<>();
            for (ArrayList<Trip> route : anArrayListRoute) {
                //получаем список остановок
                stations.add(route.get(1).getNameStartStation());
                //определяем насколько подходит остановка
                Route Route =getRoute(anArrayListRoute.get(0).get(1).getNumberRoute(),
                        anArrayListRoute.get(0).get(1).getDirectionRoute(),
                        anArrayListRoute.get(0).get(1).getDayRoute());
                if (Route != null) {
                    stationsNumber.add(Route.getStations().indexOf(route.get(1).getNameStartStation()));
                }
            }
            if (stations.size() != 0) {
                //Находим наиболее подходящую конечную остановку первого маршрута
                int maxStation = stationsNumber.get(0);
                String maxStationName = stations.get(0);
                for (int j = 1; j < stationsNumber.size(); j++) {
                    if (maxStation < stationsNumber.get(j)) {
                        maxStation = stationsNumber.get(j);
                        maxStationName = stations.get(j);
                    }
                }
                //Удаляем маршруты, которые не выходят из наиболее подходящей остановки
                for (int j = anArrayListRoute.size() - 1; j >= 0; j--) {
                    if (!anArrayListRoute.get(j).get(1).getNameStartStation().equals(maxStationName))
                        anArrayListRoute.remove(j);
                }
            }
            /**
             * Отсеиваем маршруты по конечной остановке первого маршрута
             * Находим первую по ходу движения автобуса остановку
             */
            stations = new ArrayList<>();
            stationsNumber = new ArrayList<>();
            for (ArrayList<Trip> route : anArrayListRoute) {
                //получаем список остановок
                stations.add(route.get(0).getNameFinishStation());
                //определяем насколько подходит остановка
                Route Route =getRoute(anArrayListRoute.get(0).get(0).getNumberRoute(),
                        anArrayListRoute.get(0).get(0).getDirectionRoute(),
                        anArrayListRoute.get(0).get(0).getDayRoute());
                if (Route != null) {
                    stationsNumber.add(Route.getStations().indexOf(route.get(0).getNameFinishStation()));
                }
            }
            if (stations.size() != 0) {
                //Находим наиболее подходящую конечную остановку первого маршрута
                int minStation = stationsNumber.get(0);
                String minStationName = stations.get(0);
                for (int j = 1; j < stationsNumber.size(); j++) {
                    if (minStation > stationsNumber.get(j)) {
                        minStation = stationsNumber.get(j);
                        minStationName = stations.get(j);
                    }
                }
                //Удаляем маршруты, которые не выходят из наиболее подходящей остановки
                for (int j = anArrayListRoute.size() - 1; j >= 0; j--) {
                    if (!anArrayListRoute.get(j).get(0).getNameFinishStation().equals(minStationName))
                        anArrayListRoute.remove(j);
                }
            }
            /**
             * Отсеиваем маршруты по конечной остановке
             * Находим ближайшую остановку к необходимой конечной
             */
            stations = new ArrayList<>();
            stationsNumber = new ArrayList<>();
            for (ArrayList<Trip> route : anArrayListRoute) {
                //получаем список остановок
                stations.add(route.get(1).getNameFinishStation());
                //определяем насколько подходит остановка
                stationsNumber.add(stationsC.indexOf(route.get(1).getNumberFinishStation()));
            }
            if (stations.size() != 0) {
                //Находим наиболее подходящую начальную остановку
                int minStation = stationsNumber.get(0);
                String minStationName = stations.get(0);
                for (int j = 1; j < stationsNumber.size(); j++) {
                    if (minStation > stationsNumber.get(j)) {
                        minStation = stationsNumber.get(j);
                        minStationName = stations.get(j);
                    }
                }
                //Удаляем маршруты, которые не выходят из наиболее подходящей остановки
                for (int j = anArrayListRoute.size() - 1; j >= 0; j--) {
                    if (!anArrayListRoute.get(j).get(1).getNameFinishStation().equals(minStationName))
                        anArrayListRoute.remove(j);
                }
            }
        }
        ArrayList<ArrayList<Trip>> finalArrayListRoutes=new ArrayList<>();
        for (ArrayList<ArrayList<Trip>> arrayListRoute : arrayListRoutes) {
            finalArrayListRoutes.add(arrayListRoute.get(0));
        }
        return finalArrayListRoutes;
    }
    private double distance(double lat1, double lat2, double lon1,
                            double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters
        double el = el1 - el2;
        distance = Math.pow(distance, 2) + Math.pow(el, 2);

        return Math.sqrt(distance);
    }

    //Метод определят объединены остановки в узел по номерам остановок
    private Boolean isStationsNode(int numberA, int numberB)
    {
        String nameStationA=hashMapNumberNameStation.get(numberA);
        String nameStationB=hashMapNumberNameStation.get(numberB);
        String strGpsA=gpsHashMap.get(numberA);
        String strGpsB = gpsHashMap.get(numberB);
        double distance;
        if(strGpsA!=null && strGpsB!=null) {
            String[] splitStrA = strGpsA.split(",");
            String[] splitStrB = strGpsB.split(",");
             distance = distance(
                    Double.parseDouble(splitStrA[0]),
                    Double.parseDouble(splitStrB[0]),
                    Double.parseDouble(splitStrA[1]),
                    Double.parseDouble(splitStrB[1]),
                    Double.parseDouble(splitStrA[2]),
                    Double.parseDouble(splitStrB[2]));
        }
        else
        distance=99999;
        return nameStationA.equals(nameStationB)||
                distance<=250;


    }

    private int indexOfRouteInArrayList(ArrayList<Route> arrayList, String numberRoute, String directionRoute, String dayRoute)
    {
        for(int i=0;i<arrayList.size();i++)
        {
            if(arrayList.get(i).getNumber().equals(numberRoute)&&
                    arrayList.get(i).getDirection().equals(directionRoute)&&
                    arrayList.get(i).getDay().equals(dayRoute))
                return i;
        }
        return -1;
    }
    //Функция которая подбирает первое по счету время для выбранного маршрута и остановок
    private ArrayList<String> getFirstTimeTrip(String a, String b,Route Route,String currentTime)
    {
        ArrayList<String> timeTrip = new ArrayList<>();
        int k = Route.getStations().indexOf(a);
        int m = Route.getStations().indexOf(b);
        for (int j=0;j< Route.getStationsTime().get(k).size();j++)
        {
            if (timeManager.timeComp(currentTime, Route.getStationsTime().get(k).get(j))>-1)//Проверка на то больше время или нет
            {
                timeTrip.add(Route.getStationsTime().get(k).get(j));
                timeTrip.add(Route.getStationsTime().get(m).get(j));
                break;
            }
        }

        return timeTrip;
    }
    //Функция которая подбирает время до конца дня для выбранного маршрута и остановок
    private ArrayList<ArrayList<String>> getAllTimeTrip(String a, String b,Route Route,String currentTime)
    {

        ArrayList<ArrayList<String>> timeTrip = new ArrayList<>();

        int k = Route.getStations().indexOf(a);
        int m = Route.getStations().indexOf(b);
        for (int j=0;j< Route.getStationsTime().get(k).size();j++)
        {
            if (timeManager.timeComp(currentTime, Route.getStationsTime().get(k).get(j)) !=-1)//Проверка на то больше время или нет
            {
                ArrayList<String> time= new ArrayList<>();
                time.add(Route.getStationsTime().get(k).get(j));
                time.add(Route.getStationsTime().get(m).get(j));
                timeTrip.add(time);
            }

        }
        return timeTrip;
    }
    private ArrayList<ArrayList<Trip>> findTimeForArrListRoute(ArrayList<ArrayList<Trip>> arrayListRoutes, int hour, int minute)
    {
        ArrayList<ArrayList<Trip>> finalArrayList=new ArrayList<>();
        for (ArrayList<Trip> arrayListRoute : arrayListRoutes) {
            int IDRoute = indexOfRouteInArrayList(allRoutes, arrayListRoute.get(0).getNumberRoute(),
                    arrayListRoute.get(0).getDirectionRoute(),
                    arrayListRoute.get(0).getDayRoute());
            ArrayList<ArrayList<String>> firstTime = getAllTimeTrip(
                    arrayListRoute.get(0).getNameStartStation(),
                    arrayListRoute.get(0).getNameFinishStation(),
                    allRoutes.get(IDRoute),
                    timeToString(hour, minute));
            for (ArrayList<String> aFirstTime : firstTime) {
                //Индикатор ошибки, когда время не найдено
                Boolean flagErr = false;
                ArrayList<Trip> tempAnswer = new ArrayList<>();
                if (!aFirstTime.get(0).equals("") &&
                        !aFirstTime.get(1).equals("")) {
                    tempAnswer.add(new Trip(
                            arrayListRoute.get(0).getNameStartStation(),
                            arrayListRoute.get(0).getNumberStartStation(),
                            arrayListRoute.get(0).getNameFinishStation(),
                            arrayListRoute.get(0).getNumberFinishStation(),
                            arrayListRoute.get(0).getNumberRoute(),
                            arrayListRoute.get(0).getDirectionRoute(),
                            arrayListRoute.get(0).getDayRoute(),
                            aFirstTime.get(0),
                            aFirstTime.get(1)
                    ));
                    for (int k = 1; k < arrayListRoute.size(); k++) {
                        int IDRouteSecond = indexOfRouteInArrayList(allRoutes, arrayListRoute.get(k).getNumberRoute(),
                                arrayListRoute.get(k).getDirectionRoute(),
                                arrayListRoute.get(k).getDayRoute());
                        ArrayList<String> tempTime = getFirstTimeTrip(
                                arrayListRoute.get(k).getNameStartStation(),
                                arrayListRoute.get(k).getNameFinishStation(),
                                allRoutes.get(IDRouteSecond),
                                tempAnswer.get(k - 1).getTimeStop());
                        if (tempTime.size() == 2 &&
                                !tempTime.get(0).equals("") &&
                                !tempTime.get(1).equals(""))
                            tempAnswer.add(new Trip(
                                    arrayListRoute.get(k).getNameStartStation(),
                                    arrayListRoute.get(k).getNumberStartStation(),
                                    arrayListRoute.get(k).getNameFinishStation(),
                                    arrayListRoute.get(k).getNumberFinishStation(),
                                    arrayListRoute.get(k).getNumberRoute(),
                                    arrayListRoute.get(k).getDirectionRoute(),
                                    arrayListRoute.get(k).getDayRoute(),
                                    tempTime.get(0),
                                    tempTime.get(1)
                            ));
                        else {
                            flagErr = true;
                            break;
                        }
                    }
                } else
                    flagErr = true;
                if (!flagErr)
                    finalArrayList.add(tempAnswer);
            }
        }
        return finalArrayList;
    }
    private String timeToString(Integer h, Integer m)
    {
        String currentTime = "";
        if (h < 10)
        {
            currentTime += "0" + h;
        }
        else
        {
            currentTime += h;
        }
        currentTime += ":";
        if (m < 10)
        {
            currentTime += "0" + m;
        }
        else
        {
            currentTime += m;
        }
        return currentTime;
    }
    //Метод, который возвращает список остановок
    private ArrayList<String> getAllStationNames()
    {
        ArrayList<String> allStationNames=new ArrayList<>();
        for(int numberStation: getAllNumberStations())
        {
            if(!allStationNames.contains(hashMapNumberNameStation.get(numberStation)))
                allStationNames.add(hashMapNumberNameStation.get(numberStation));
        }
        Collections.sort(allStationNames);
        return allStationNames;
    }
    //Метод, который возвращает номера остановки по ее названию
    public ArrayList<Integer> getArrayListStationNumbers(String stationName)
    {
        ArrayList<Integer> stationNumbers=new ArrayList<>();
        for(int numberStation: getAllNumberStations())
        {
            if(hashMapNumberNameStation.get(numberStation).equals(stationName))
                stationNumbers.add(numberStation);
        }
        Collections.sort(getAllNumberStations());
        return stationNumbers;
    }
    //Метод возвращает класс маршрута
    public Route getRoute(String number, String direction, String day)
    {
        for(Route Route : allRoutes)
        if(Route.getNumber().equals(number)&&
                Route.getDirection().equals(direction)&&
                Route.getDay().equals(day))
            return Route;
        return null;
    }
    //Метод возвращает список всех имен остановок в алфавитном порядке
    public ArrayList<String> getArrayListNameStations() {
        ArrayList<String> allNameStations=new ArrayList<>();
        for(Integer numberStation: getAllNumberStations())
        {
            String tempName=hashMapNumberNameStation.get(numberStation);
            if(!allNameStations.contains(tempName))
                allNameStations.add(tempName);
        }
        Collections.sort(allNameStations);
        return allNameStations;
    }
    public ArrayList<ArrayList<String>> getNextStationForNumbers(ArrayList<Integer> numbers)
    {
        ArrayList<ArrayList<String>> listNextStations = new ArrayList<>();
        //Для каждого номера остановки ищем названия остановок следующих после данной
        for(int number : numbers)
        {
            ArrayList<String> tempListNextStations = new ArrayList<>();
            for(Route route:allRoutes)
            {
                for(int j = 0; j < route.getStationsNumber().size() - 1; j++)
                {
                    if(route.getStationsNumber().get(j)==number &&
                            !tempListNextStations.contains(route.getStations().get(j+1)))
                        tempListNextStations.add(route.getStations().get(j+1));
                }
            }
            listNextStations.add(tempListNextStations);
        }
        return listNextStations;
    }
    public ArrayList<Integer> getAllNumberStations() {
        return allNumberStations;
    }
    public ArrayList<String> getAllNumberRoutes()
    {
        return allNumberRoutes;
    }
    //метод принимает название остановки, номер остановки, день (рабочий или выходной)
    //возвращает расписание остановки в виже класса StationTimetable
    public StationTimetable getStationTimetable(String station, int numberStation, Integer day, Integer month, Integer year, Integer hour, Integer minute)
    {
        Calendar nowTime = new GregorianCalendar(year, month - 1, day, hour, minute);
        //Добавляем ко времени отклонение от расписания на 3 минуты
        nowTime.add(Calendar.MINUTE, -3);
        year = nowTime.get(Calendar.YEAR);
        month = nowTime.get(Calendar.MONTH) + 1;
        day = nowTime.get(Calendar.DAY_OF_MONTH);
        hour = nowTime.get(Calendar.HOUR_OF_DAY);
        minute = nowTime.get(Calendar.MINUTE);
        String currentTime = timeToString(hour, minute);
        Calendar dateTime = new GregorianCalendar(year, month - 1, day);
        //Если время от 0 до 2 часов ночи, то автобысы ходят по рсписанию вчерашнего дня)
        if(hour < 2 && hour >= 0)
        {
            dateTime.add(Calendar.HOUR, -24);
        }
        //Класс для хранения ответов
        StationTimetable stationTimetable = new StationTimetable(station, numberStation);
        //Ищем в списке строки, в которых совпадают название остановки
        ArrayList<ArrayList<TimetableRouteOnStation>> arrayTimetableRoutes = new ArrayList<>();
        //Время когда будет последний автобус
        String maxTime = "03:00";
        for(Route route:allRoutes)
        {
            for(int j=0;j< route.getStationsNumber().size();j++) {
                if (numberStation == route.getStationsNumber().get(j)) {
                    Boolean flag = false;
                    //В цикле идет проверка на то есть ли уже данный автобус в расписании, если он есть, то добавляем к существующему списку
                    for (ArrayList<TimetableRouteOnStation> arrayTimetableRoute : arrayTimetableRoutes) {
                        if (arrayTimetableRoute.get(0).getNumber().equals(route.getNumber()) &&
                                arrayTimetableRoute.get(0).getDirection().equals(route.getDirection())) {
                            if (timeManager.isRightDay(route.getDay(), dateTime.get(Calendar.YEAR),
                                    dateTime.get(Calendar.MONTH) + 1, dateTime.get(Calendar.DAY_OF_MONTH))) {
                                String tempMaxTime = getMaxTime(route.getStationsTime().get(j));
                                if (timeManager.timeComp(tempMaxTime, maxTime) == -1)
                                    maxTime = tempMaxTime;
                            }
                            ArrayList<String> tempList = new ArrayList<>(route.getStationsTime().get(j));
                            tempList.removeAll(Arrays.asList("", null));
                            arrayTimetableRoute.add(
                                    new TimetableRouteOnStation(route.getNumber(),
                                            route.getDirection(),
                                            route.getDay(),
                                            tempList));
                            flag = true;
                            break;
                        }
                    }
                    //Если значение флага false, то создаем новый список для расписания
                    if (!flag) {
                        ArrayList<TimetableRouteOnStation> tempArrayList = new ArrayList<>();
                        ArrayList<String> tempList = new ArrayList<>(route.getStationsTime().get(j));
                        tempList.removeAll(Arrays.asList("", null));
                        tempArrayList.add(new TimetableRouteOnStation(route.getNumber(),
                                route.getDirection(),
                                route.getDay(),
                                tempList));
                        arrayTimetableRoutes.add(tempArrayList);
                        if (timeManager.isRightDay(route.getDay(), dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH) + 1,
                                dateTime.get(Calendar.DAY_OF_MONTH))) {
                            String tempMaxTime = getMaxTime(route.getStationsTime().get(j));
                            if (timeManager.timeComp(tempMaxTime, maxTime) == -1)
                                maxTime = tempMaxTime;
                        }
                    }
                }
            }
        }
        Collections.sort(arrayTimetableRoutes, new Comparator<ArrayList<TimetableRouteOnStation>>()
        {
            @Override
            public int compare(ArrayList<TimetableRouteOnStation> lhs, ArrayList<TimetableRouteOnStation> rhs)
            {

                String templhs = lhs.get(0).getNumber().replaceAll("\\D", "");
                String temprhs = rhs.get(0).getNumber().replaceAll("\\D", "");
                Integer left = Integer.parseInt(templhs);
                Integer right = Integer.parseInt(temprhs);
                return left.compareTo(right);
            }
        });

        stationTimetable.addRoutes(arrayTimetableRoutes);

        //Обновляем текущее время по исходным параметрам
        dateTime = new GregorianCalendar(year, month - 1, day);
        //Если последний автобус уехал, то нужно показать расписание на утро
        if(timeManager.timeComp(currentTime, maxTime) == -1)
        {
            //Если время до 00:00 нужно переставить дату на следующий день, если после то дата остается той же
            if(hour >= 2 && hour < 24)
                dateTime.add(Calendar.HOUR, 24);
            //Устанавливаем время для поиска маршрутов утром
            hour = 2;
            minute = 0;
        }
        //Если еще есть рейсы
        else
        {
            //Если время после 00 часов, то автобусы ездят по расписанию вчерашнего дня
            if(hour < 2 && hour >= 0)
            {
                dateTime.add(Calendar.HOUR, -24);
            }
        }
        stationTimetable.setDayComment(timeManager.getCommentForDay(dateTime.get(Calendar.YEAR),
                dateTime.get(Calendar.MONTH) + 1,
                dateTime.get(Calendar.DAY_OF_MONTH)));
        //теперь получаем необходимую раскраску времени
        for(int i = 0; i < stationTimetable.getRoutes().size(); i++)
        {
            for(int j = 0; j < stationTimetable.getRoutes().get(i).size(); j++)
            {
                //Если день подходит, то раскрашиваем в нужные цветва
                if(timeManager.isRightDay(stationTimetable.getRoutes().get(i).get(j).getDay(), dateTime.get(Calendar.YEAR),
                        dateTime.get(Calendar.MONTH) + 1, dateTime.get(Calendar.DAY_OF_MONTH)))
                {
                    stationTimetable.getRoutes().get(i).get(j).setColorTime(getColorsForTime(
                            stationTimetable.getRoutes().get(i).get(j).getTime(), hour, minute));
                }
                //Иначе раскрашиваем в черный цвет
                else
                {
                    ArrayList<Integer> colors = new ArrayList<>();
                    for(Integer t = 0; t < stationTimetable.getRoutes().get(i).get(j).getTime().size(); t++)
                        colors.add(0);
                    stationTimetable.getRoutes().get(i).get(j).setColorTime(colors);

                }
            }
        }

        return stationTimetable;
        //Возвращаем результат
    }
    //Метод возвращает максимальное время в списке
    private String getMaxTime(ArrayList<String> time)
    {
        String maxTime = "03:00";
        for(String aTime : time)
        {
            if(timeManager.timeComp(aTime, maxTime) == -1)
                maxTime = aTime;
        }
        return maxTime;
    }
    //Метод вычисляет цвет для времени в расписании
    //0 - черный
    //1 - красный
    //2 - зеленый
    //3 - жирный зеленый
    private ArrayList<Integer> getColorsForTime(ArrayList<String> time, Integer hour, Integer minute)
    {
        String currentTime = "";
        if(hour < 10)
        {
            currentTime += "0" + hour;
        } else
        {
            currentTime += hour;
        }
        currentTime += ":";
        if(minute < 10)
        {
            currentTime += "0" + minute;
        } else
        {
            currentTime += minute;
        }
        ArrayList<Integer> colors = new ArrayList<>();
        Boolean isNewDay = true;
        for(String aTime : time)
        {
            if(timeManager.timeComp(currentTime, aTime) == -1)
            {
                colors.add(1);
            } else
            {
                if(isNewDay)
                {
                    isNewDay = false;
                    colors.add(3);
                } else
                {
                    colors.add(2);
                }
            }
        }
        return colors;
    }
    //Метод возвращает расписание маршрута
    public RouteTimetable getAllRouteTimetable(String number, String direction, int year, int month, int day, int hour, int minute)
    {
        Calendar nowTime = new GregorianCalendar(year, month - 1, day, hour, minute);
        //Добавляем отклонение от расписания на 3 минуты
        nowTime.add(Calendar.MINUTE, -3);
        year = nowTime.get(Calendar.YEAR);
        month = nowTime.get(Calendar.MONTH) + 1;
        day = nowTime.get(Calendar.DAY_OF_MONTH);
        hour = nowTime.get(Calendar.HOUR_OF_DAY);
        minute = nowTime.get(Calendar.MINUTE);
        String currentTime = timeToString(hour, minute);
        Calendar dateTime = new GregorianCalendar(year, month - 1, day);
        if(hour < 2 && hour >= 0)
        {
            dateTime.add(Calendar.HOUR, -24);
        }
        //Время когда будет последний автобус
        String maxTime = "03:00";
        RouteTimetable tempTimetableRoute;
        tempTimetableRoute = new RouteTimetable(number, direction);

        ArrayList<Route> routes = new ArrayList<>();
        for(Route route:allRoutes)
        {
            if(route.getNumber().equals(number)&&
                    route.getDirection().equals(direction))
                routes.add(route);
        }
        ArrayList<ArrayList<TimetableStationOnRoute>> arrayTimetableRoutes=new ArrayList<>();
        for(Route route:routes)
        {
            for(int j=0;j< route.getStationsNumber().size();j++) {
                    Boolean flag = false;
                    //В цикле идет проверка на то есть ли уже данная остановка в расписании, если она есть, то добавляем к существующему списку
                    for (ArrayList<TimetableStationOnRoute> arrayTimetableRoute : arrayTimetableRoutes) {
                        if (arrayTimetableRoute.get(0).getStationName().equals(route.getStations().get(j)) &&
                                arrayTimetableRoute.get(0).getStationNumber()==route.getStationsNumber().get(j)) {
                            if (timeManager.isRightDay(route.getDay(), dateTime.get(Calendar.YEAR),
                                    dateTime.get(Calendar.MONTH) + 1, dateTime.get(Calendar.DAY_OF_MONTH))) {
                                String tempMaxTime = getMaxTime(route.getStationsTime().get(j));
                                if (timeManager.timeComp(tempMaxTime, maxTime) == -1)
                                    maxTime = tempMaxTime;
                            }
                            ArrayList<String> tempList = new ArrayList<>(route.getStationsTime().get(j));
                            tempList.removeAll(Arrays.asList("", null));
                            arrayTimetableRoute.add(
                                    new TimetableStationOnRoute(route.getStations().get(j),
                                            route.getStationsNumber().get(j),
                                            route.getDay(),
                                            tempList));
                            flag = true;
                            break;
                        }
                    }
                    //Если значение флага false, то создаем новый список для расписания
                    if (!flag) {
                        ArrayList<TimetableStationOnRoute> tempArrayList = new ArrayList<>();
                        ArrayList<String> tempList = new ArrayList<>(route.getStationsTime().get(j));
                        tempList.removeAll(Arrays.asList("", null));
                        tempArrayList.add(new TimetableStationOnRoute(route.getStations().get(j),
                                route.getStationsNumber().get(j),
                                route.getDay(),
                                tempList));
                        arrayTimetableRoutes.add(tempArrayList);
                        if (timeManager.isRightDay(route.getDay(), dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH) + 1,
                                dateTime.get(Calendar.DAY_OF_MONTH))) {
                            String tempMaxTime = getMaxTime(route.getStationsTime().get(j));
                            if (timeManager.timeComp(tempMaxTime, maxTime) == -1)
                                maxTime = tempMaxTime;
                        }
                    }
            }
        }
        tempTimetableRoute.setStations(arrayTimetableRoutes);
        //Обновляем текущее время по исходным параметрам
        dateTime = new GregorianCalendar(year, month - 1, day);
        //Если последний автобус уехал, то нужно показать расписание на утро
        if(timeManager.timeComp(currentTime, maxTime) == -1)
        {
            //Если время до 00:00 нужно переставить дату на следующий день, если после то дата остается той же
            if(hour >= 2 && hour < 24)
                dateTime.add(Calendar.HOUR, 24);
            //Устанавливаем время для поиска маршрутов утром
            hour = 2;
            minute = 0;
        }
        //Если еще есть рейсы
        else
        {
            //Если время после 00 часов, то автобусы ездят по расписанию вчерашнего дня
            if(hour < 2 && hour >= 0)
            {
                dateTime.add(Calendar.HOUR, -24);
            }
        }
        tempTimetableRoute.setDayComment(timeManager.getCommentForDay(dateTime.get(Calendar.YEAR),
                dateTime.get(Calendar.MONTH) + 1,
                dateTime.get(Calendar.DAY_OF_MONTH)));
        //теперь получаем необходимую раскраску времени
        for(int i = 0; i < tempTimetableRoute.getStations().size(); i++)
        {
            for(int j = 0; j < tempTimetableRoute.getStations().get(i).size(); j++)
            {
                //Если день подходит, то раскрашиваем в нужные цветва
                if(timeManager.isRightDay(tempTimetableRoute.getStations().get(i).get(j).getDayRoute(), dateTime.get(Calendar.YEAR),
                        dateTime.get(Calendar.MONTH) + 1, dateTime.get(Calendar.DAY_OF_MONTH)))
                {
                    tempTimetableRoute.getStations().get(i).get(j).setColorsTimesRoute(getColorsForTime(
                            tempTimetableRoute.getStations().get(i).get(j).getTimesRoute(), hour, minute));
                }
                //Иначе раскрашиваем в черный цвет
                else
                {
                    ArrayList<Integer> colors = new ArrayList<>();
                    for(int t = 0; t < tempTimetableRoute.getStations().get(i).get(j).getTimesRoute().size(); t++)
                        colors.add(0);
                    tempTimetableRoute.getStations().get(i).get(j).setColorsTimesRoute(colors);

                }
            }
        }
        return tempTimetableRoute;
        //Возвращаем результат
    }

    public HashMap<Integer, String> getHashMapNumberNameStation() {
        return hashMapNumberNameStation;
    }

    public HashMap<Integer, String> getGpsHashMap() {
        return gpsHashMap;
    }
}


