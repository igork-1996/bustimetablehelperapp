package com.igorkondrashuk.bustimetablehelper.routemanager;

import java.util.ArrayList;

/**
 * Класс для хранения одной вариации поездки
 */
public class RouteAnswer {
    //Время поездки (минуты)
    private int tripTime;
    //Время с момента запроса до времени прибытия (минуты)
    private int allTime;
    //Массив маршрутов для поездки
    private ArrayList<Trip> arrayListTrip;
    RouteAnswer(ArrayList<Trip> arrayListTrip,int tripTime,int allTime)
    {
        this.arrayListTrip=arrayListTrip;
        this.tripTime=tripTime;
        this.allTime=allTime;
    }
    public int getTripTime()
    {
        return tripTime;
    }
    public int getAllTime()
    {
        return allTime;
    }
    public ArrayList<Trip> getArrayListTrip()
    {
        return arrayListTrip;
    }
}
