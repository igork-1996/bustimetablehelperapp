
package com.igorkondrashuk.bustimetablehelper.routemanager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
/*
    Класс для работы со временем
 */
public class TimeManager
{

    private ArrayList<Integer> yearArrayList;
    private ArrayList<Integer> monthArrayList;
    private ArrayList<Integer> dayArrayList;
    private ArrayList<Boolean> isHolidayArrayList;
    private ArrayList<String> reasonArrayList;
    public TimeManager(ArrayList<Integer> yearArrayList,
                ArrayList<Integer> monthArrayList,
                ArrayList<Integer> dayArrayList,
                ArrayList<Boolean> isHolidayArrayList,
                ArrayList<String> reasonArrayList)
    {
        this.yearArrayList = yearArrayList;
        this.monthArrayList = monthArrayList;
        this.dayArrayList = dayArrayList;
        this.isHolidayArrayList = isHolidayArrayList;
        this.reasonArrayList = reasonArrayList;
    }
    private Boolean isHoliday(Integer year, Integer month, Integer day)
    {
        for(int i=0;i<yearArrayList.size();i++)
        {
            if(yearArrayList.get(i).equals(year) &&
                    monthArrayList.get(i).equals(month) &&
                    dayArrayList.get(i).equals(day))
            {
                return isHolidayArrayList.get(i);
            }
        }
        Calendar date = new GregorianCalendar(year,month-1,day);
        return date.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || date.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }
    String getCommentForDay(Integer year, Integer month, Integer day) {
        Calendar date = new GregorianCalendar(year, month - 1, day);
        String strDayOfWeek="";
        switch (date.get(Calendar.DAY_OF_WEEK))
        {
            case Calendar.MONDAY:
                strDayOfWeek="понедельник";
                break;
            case Calendar.TUESDAY:
                strDayOfWeek="вторник";
                break;
            case Calendar.WEDNESDAY:
                strDayOfWeek="среда";
                break;
            case Calendar.THURSDAY:
                strDayOfWeek="четверг";
                break;
            case Calendar.FRIDAY:
                strDayOfWeek="пятница";
                break;
            case Calendar.SATURDAY:
                strDayOfWeek="суббота";
                break;
            case Calendar.SUNDAY:
                strDayOfWeek="воскресенье";
                break;
        }

        String comment = "";
        comment+=day+" ";
        switch (date.get(Calendar.MONTH))
        {
            case Calendar.JANUARY:
                comment+="января";
                break;
            case Calendar.FEBRUARY:
                comment+="февраля";
                break;
            case Calendar.MARCH:
                comment+="марта";
                break;
            case Calendar.APRIL:
                comment+="апреля";
                break;
            case Calendar.MAY:
                comment+="мая";
                break;
            case Calendar.JUNE:
                comment+="июня";
                break;
            case Calendar.JULY:
                comment+="июля";
                break;
            case Calendar.AUGUST:
                comment+="августа";
            break;
            case Calendar.SEPTEMBER:
                comment+="сентября";
                break;
            case Calendar.OCTOBER:
                comment+="октября";
                break;
            case Calendar.NOVEMBER:
                comment+="ноября";
                break;
            case Calendar.DECEMBER:
                comment+="декабря";
                break;
        }

        comment+=" "+year+" года, "+strDayOfWeek+"\n";
        for (int i = 0; i < yearArrayList.size(); i++)
        {
            if (yearArrayList.get(i).equals(year) &&
                    monthArrayList.get(i).equals(month) &&
                    dayArrayList.get(i).equals(day))
            {
                if (isHolidayArrayList.get(i))
                {
                    comment += "выходной день, ";
                }
                else
                {
                    comment += "рабочий день, ";
                }
                comment += reasonArrayList.get(i);
                return comment;
            }
        }
        if (isHoliday(year, month, day))
            comment += "выходной день";
        else
            comment += "рабочий день";
        return comment;
    }
    Boolean isRightDay(String routeDay, Integer year, Integer month, Integer day)
    {
        String tempDay="день недели";
        Calendar today = new GregorianCalendar(year, month-1, day);
        switch(today.get(Calendar.DAY_OF_WEEK))
        {
            case Calendar.MONDAY:
                tempDay = "ПН";
                break;
            case Calendar.TUESDAY:
                tempDay = "ВТ";
                break;
            case Calendar.WEDNESDAY:
                tempDay = "СР";
                break;
            case Calendar.THURSDAY:
                tempDay = "ЧТ";
                break;
            case Calendar.FRIDAY:
                tempDay = "ПТ";
                break;
            case Calendar.SATURDAY:
                tempDay = "СБ";
                break;
            case Calendar.SUNDAY:
                tempDay = "ВС";
                break;
        }
        String nowDay;
        if (isHoliday(year, month, day))
            nowDay = "В";
        else
            nowDay = "Р";
        String orday = "Р,В";
        if (routeDay.equals(nowDay) || routeDay.equals(orday))
            return true;
        else
        {
            if (routeDay.contains(tempDay))
                return true;
        }
        return false;
    }
    //функция для сравнения времени
    //Значение -1 если a>b
    //Значение положительное a<b и возвращается разница во времени
    Integer timeComp(String a, String b)
    {
        //TODO реализовать работу со временем с помощью regex
        if (a ==null || b ==null|| a.equals("") || b.equals( ""))
            return -2;
        if(a.length()<5)
        {
            a="0"+a;
        }
        if (b.length() < 5)
        {
            b = "0" + b;
        }
        String sH1="";

        sH1 +=""+ a.charAt(0) + a.charAt(1);
        int h1= Integer.parseInt(sH1);
        String sM1 = "";
        sM1 += ""+ a.charAt(3) + a.charAt(4);
        int m1 = Integer.parseInt(sM1);
        String sH2 = "";
        sH2 += "" + b.charAt(0) + b.charAt(1);
        int h2 = Integer.parseInt(sH2);
        String sM2 = "";
        sM2 += "" + b.charAt(3) + b.charAt(4);
        int m2 = Integer.parseInt(sM2);
        if (h1 < 2) h1 += 24;
        if (h2 < 2) h2 += 24;
        int dM = h2 * 60 + m2 - h1 * 60 - m1;
        if (dM < 0)
            return -1;
        else
            return dM;
    }
}

