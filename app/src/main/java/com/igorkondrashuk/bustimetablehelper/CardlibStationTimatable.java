package com.igorkondrashuk.bustimetablehelper;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Класс карточка для представления расписания остановки
 */
public class CardlibStationTimatable extends Card {
    protected TextView mTitle;
    protected TextView time;
    SpannableStringBuilder sb;
    String numberRoute;
    public CardlibStationTimatable(Context context,String numberRoute,SpannableStringBuilder sb) {
        super(context,R.layout.cardlib_station_timetable);
        this.sb=sb;
        this.numberRoute=numberRoute;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);
        mTitle=(TextView)parent.findViewById(R.id.textNumberRoute);
        mTitle.setText(numberRoute);
        time=(TextView)parent.findViewById(R.id.textTime);
        time.setText(sb);
    }
}
