package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Фрагмент выбора даннах для поиска маршрута
 */
public class ChoiceFindRouteDataFragment extends Fragment implements TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener {
    private View rootview;
    //Выбранная начальная остановка
    private TextView firstStation;
    //Выбранная конечная остановка
    private TextView secondStaion;
    //Календарь для работы со временем
    private Calendar now;
    //Строки для хранения выбранных остановок
    private String stringFirstStaion;
    private String stringSecondStation;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.choice_find_route_data_fragment,container,false);
        if(now==null) {
            //Инициализация текущего времени
            now = Calendar.getInstance();
        }
        //Установка слущателя для кнопки выбора времени
        TextView textViewDate = (TextView) rootview.findViewById(R.id.textViewData);
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ChoiceFindRouteDataFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.vibrate(false);
                dpd.setAccentColor(Color.parseColor("#1565C0"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
        //Установка слушателя для кнопки выбора даты
        TextView textViewTime = (TextView) rootview.findViewById(R.id.textViewTime);
        textViewTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        ChoiceFindRouteDataFragment.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        true
                );
                tpd.setAccentColor(Color.parseColor("#1565C0"));
                tpd.vibrate(false);
                tpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
        //Установка слушателя для выбора первой остановки
        firstStation=(TextView)rootview.findViewById(R.id.textViewFirstStation);
        firstStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choiceStationListener.someEvent("1.2", "1");
            }
        });
        //Установка слушателя для выбора второй остановки
        secondStaion=(TextView)rootview.findViewById(R.id.textViewSecondStation);
        secondStaion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choiceStationListener.someEvent("1.2", "2");
            }
        });
        //Установка слушателя для кнопки смены конечных отановок
        ImageButton imageButton=(ImageButton)rootview.findViewById(R.id.imageButtonSwap);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView1 = (TextView) rootview.findViewById(R.id.textViewFirstStation);
                TextView textView2 = (TextView) rootview.findViewById(R.id.textViewSecondStation);
                String temp = textView1.getText().toString();
                stringFirstStaion=textView2.getText().toString();
                textView1.setText(stringFirstStaion);
                stringSecondStation=temp;
                textView2.setText(stringSecondStation);
            }
        });
        //Установка слушателя для кнопки поиска
        ImageButton fndButton = (ImageButton) rootview.findViewById(R.id.imageButton);
        fndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textViewFirstStation = (TextView) rootview.findViewById(R.id.textViewFirstStation);
                TextView textViewSecondStation = (TextView) rootview.findViewById(R.id.textViewSecondStation);
                findButtonEventListener.findDataChoiced("3",
                        textViewFirstStation.getText().toString(),
                        textViewSecondStation.getText().toString(),
                        now);
            }
        });
        //Устанавливаем класс с реализацией интерфейса для нажатия кнопки выбора остановки
        choiceStationListener=(onFindButtonEventListener)getActivity();
        //Устанавливаем класс с реализации интерфейса для нажатия кнопки поиска
        findButtonEventListener=(findDataListener)getActivity();
        //Установка текущего времени в качестве выбранного для поиска
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",Locale.ENGLISH);
        String time = sdf.format(now.getTime());
        textViewTime.setText(time);
        
        setDayOfWeek(now.get(Calendar.DAY_OF_MONTH),now.get(Calendar.MONTH),now.get(Calendar.DAY_OF_WEEK));
        //Установка слушателя для кнопки обновления текущего времени и даты
        ImageButton imageButtonUpdateTime=(ImageButton)rootview.findViewById(R.id.imageButtonUpdateTime);
        imageButtonUpdateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                now = Calendar.getInstance();
                TextView textView2=(TextView)rootview.findViewById(R.id.textViewTime);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
                String tempTime = sdf.format(now.getTime());
                textView2.setText(tempTime);
                setDayOfWeek(now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_WEEK));
            }
        });
        //Установка остановок для поиска по умолчанию
        if(stringFirstStaion==null&&stringSecondStation==null)
        {

            stringFirstStaion="ЦУМ";
            stringSecondStation="ЦМТ";
        }
        firstStation.setText(stringFirstStaion);
        secondStaion.setText(stringSecondStation);
        SharedPreferences sharedPref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper", "null");
        final Gson parser=new Gson();
        //Загрузка класса из SharedPreferences
        final SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
        }.getType());
        TextView textView = (TextView) rootview.findViewById(R.id.idempty);
        if(sharedPreferencesHelper.getFavoriteStartStationNames().size()!=0) {
            textView.setVisibility(View.INVISIBLE);
        }
        else
        {
            textView.setVisibility(View.VISIBLE);
        }
        final SelectedFindRouteListAdapter selectedFindRouteListAdapter=new SelectedFindRouteListAdapter(
                getActivity(),
                sharedPreferencesHelper.getFavoriteStartStationNames(),
                sharedPreferencesHelper.getFavoriteFinishStationNames());

        LinearLayout linearLayout=(LinearLayout)rootview.findViewById(R.id.myListLinear);
        final int adapterCount = selectedFindRouteListAdapter.getCount();
        for (int  itemNumber = 0; itemNumber < adapterCount; itemNumber++) {
            final View item = selectedFindRouteListAdapter.getView(itemNumber, null, null);
           item.setTag(Integer.toString(itemNumber));
            item.setBackgroundDrawable(getResources().getDrawable(R.drawable.text_background_touch));
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str=(String)v.getTag();
                    firstStation.setText(selectedFindRouteListAdapter.getFirstStation().get(Integer.parseInt(str)));
                    secondStaion.setText(selectedFindRouteListAdapter.getSecondStation().get(Integer.parseInt(str)));
                    ScrollView myScrollView=(ScrollView)rootview.findViewById(R.id.scrollView);
                    myScrollView.pageScroll(View.FOCUS_UP);
                }
            });
            linearLayout.addView(item);
           if(itemNumber != adapterCount-1)
           {
               ImageView divider = new ImageView(getActivity());
               LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);
               lp.setMargins(0, 0, 0, 0);
               divider.setLayoutParams(lp);
               //divider.setBackgroundColor(Color.WHITE);
               divider.setBackgroundColor(Color.LTGRAY);
               linearLayout.addView(divider);
           }
        }
        return rootview;
    }
    //Метод принимает назваие выбранной начальной остановки
    public void setFirstStation(String strFirstStation)
    {
        stringFirstStaion=strFirstStation;
    }
    //Метод принимает назваие выбранной конечной остановки остановки
    public void setSecondStaionStation(String strSecondStation)
    {
        stringSecondStation=strSecondStation;
    }
    //Метод вызывается, когда выбирается новая дата в диалгое
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar=Calendar.getInstance();
        calendar.set(year,monthOfYear,dayOfMonth);
        now.set(year,monthOfYear,dayOfMonth);
        setDayOfWeek(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_WEEK));
        now.set(year, monthOfYear, dayOfMonth);

    }
    //Метод вызывается когда выбирается ново время в диалоге
    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        TextView timeTextView=(TextView)rootview.findViewById(R.id.textViewTime);
        now.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH),hourOfDay,minute,second);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String time = sdf.format(now.getTime());
        timeTextView.setText(time);
    }
    //Интерфейс для нажатия кнопик поиска
    public interface findDataListener {
        void findDataChoiced(String tagFragment, String stringFirstStation, String secondStation, Calendar now);
    }
    findDataListener findButtonEventListener;
    //Интерфейс для нажатия на кнопку выбора остановки
    public interface onFindButtonEventListener {
        void someEvent(String tagFragment,String stringSelected);
    }
    onFindButtonEventListener choiceStationListener;
    //Метод устанавливает указанную дату для поииска
    private void setDayOfWeek(int monthDay,int monthNumber,int numberDay)
    {
        TextView dateTextView=(TextView)rootview.findViewById(R.id.textViewData);
        String date="";
        date+=monthDay+" ";
        switch(monthNumber)
        {
            case 0:
                date+="янв";
                break;
            case 1:
                date+="фев";
                break;
            case 2:
                date+="мар";
                break;
            case 3:
            date+="апр";
            break;
            case 4:
                date+="май";
                break;
            case 5:
                date+="июн";
                break;
            case 6:
                date+="июл";
                break;
            case 7:
                date+="авг";
                break;
            case 8:
                date+="сен";
                break;
            case 9:
            date+="окт";
            break;
            case 10:
            date+="ноя";
            break;
            case 11:
            date+="дек";
            break;
        }
        date+=" ";
        switch(numberDay)
        {
            case 2:
                date+="пн";
                break;
            case 3:
                date+="вт";
                break;
            case 4:
                date+="ср";
                break;
            case 5:
                date+="чт";
                break;
            case 6:
                date+="пт";
                break;
            case 7:
                date+="сб";
                break;
            case 1:
            date+="вс";
            break;
        }
        dateTextView.setText(date);
    }
}
