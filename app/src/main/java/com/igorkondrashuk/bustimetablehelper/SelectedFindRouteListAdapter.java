package com.igorkondrashuk.bustimetablehelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Класс адаптер для отображения избранных вариантов для поиска
 */

public class SelectedFindRouteListAdapter extends BaseAdapter {
    ArrayList<String> firstStation;
    ArrayList<String> secondStation;
    Activity context;
    SharedPreferences sharedPref;
    public SelectedFindRouteListAdapter(Activity context, ArrayList<String> firstStation, ArrayList<String> secondStation) {
        this.firstStation = firstStation;
        this.secondStation = secondStation;
        this.context=context;
        sharedPref= context.getSharedPreferences("pref", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return firstStation.size();
    }

    @Override
    public Object getItem(int position) {
        return firstStation.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView,final ViewGroup parent) {
        if(convertView==null)
            convertView=context.getLayoutInflater().inflate(R.layout.selected_find_route, parent, false);

        TextView textViewStart=(TextView)convertView.findViewById(R.id.textViewStartStation);
        TextView textViewStop=(TextView)convertView.findViewById(R.id.textViewStopStation);
        textViewStart.setText(firstStation.get(position));
        textViewStop.setText(secondStation.get(position));
        final ImageButton imageButtonStar=(ImageButton)convertView.findViewById(R.id.imageButtonStar);
        imageButtonStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jsonSharedPreferencesHelper = sharedPref.getString("shared_preferences_helper", "null");
                Gson parser = new Gson();
                //Загрузка класса из SharedPreferences
                SharedPreferencesHelper sharedPreferencesHelper = parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
                }.getType());
                if (sharedPreferencesHelper.isFavoriteFindStations(firstStation.get(position), secondStation.get(position))) {
                    sharedPreferencesHelper.deleteFavoriteFindStations(firstStation.get(position), secondStation.get(position));
                    imageButtonStar.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_border_star_gold_65));
                } else {
                    sharedPreferencesHelper.addFavoriteFindStations(firstStation.get(position), secondStation.get(position));
                    imageButtonStar.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_full_star_gold_65));
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                editor.apply();
            }
        });
        return convertView;
    }

    public ArrayList<String> getFirstStation() {
        return firstStation;
    }

    public ArrayList<String> getSecondStation() {
        return secondStation;
    }
}
