package com.igorkondrashuk.bustimetablehelper;

import java.util.ArrayList;

/**
 * Класс для сохранения избранных данных в SharedPreferences
 */
public class SharedPreferencesHelper {
    private ArrayList<String> favoriteStationNames;
    private ArrayList<Integer> favoriteStationNumbers;

    private ArrayList<String> favoriteRouteNumbers;
    private ArrayList<String> favoriteRouteDirections;

    private ArrayList<String> favoriteStartStationNames;
    private ArrayList<String> favoriteFinishStationNames;

    public SharedPreferencesHelper() {
        favoriteStationNames=new ArrayList<>();
        favoriteStationNumbers=new ArrayList<>();
        favoriteRouteNumbers=new ArrayList<>();
        favoriteRouteDirections=new ArrayList<>();
        favoriteStartStationNames=new ArrayList<>();
        favoriteFinishStationNames=new ArrayList<>();
    }
    public void addFavoriteStation(String stationName, int stationNumber)
    {
        favoriteStationNames.add(stationName);
        favoriteStationNumbers.add(stationNumber);
    }
    public void deleteFavoriteStation(String stationName, int stationNumber)
    {
        for(int i=favoriteStationNames.size()-1;i>=0;i--)
        {
            if(favoriteStationNames.get(i).equals(stationName)&&
                    favoriteStationNumbers.get(i)==stationNumber) {
                favoriteStationNames.remove(i);
                favoriteStationNumbers.remove(i);
            }
        }
    }
    public Boolean isFavoriteStation(String stationName, int stationNumber)
    {
        for(int i=0;i<favoriteStationNames.size();i++)
        {
            if(favoriteStationNames.get(i).equals(stationName)&&
                    favoriteStationNumbers.get(i)==stationNumber)
                return true;
        }
        return false;
    }
    public void addFavoriteRoute(String routeNumber, String routeDirection)
    {
        favoriteRouteNumbers.add(routeNumber);
        favoriteRouteDirections.add(routeDirection);
    }
    public void deleteFavoriteRoute(String routeNumber, String routeDiretion)
    {
        for(int i=favoriteRouteNumbers.size()-1;i>=0;i--)
        {
            if(favoriteRouteNumbers.get(i).equals(routeNumber)&&
                    favoriteRouteDirections.get(i).equals(routeDiretion)) {
                favoriteRouteNumbers.remove(i);
                favoriteRouteDirections.remove(i);
            }
        }
    }
    public Boolean isFavoriteRoute(String routeNumber, String routeDiretion)
    {
        for(int i=0;i<favoriteRouteNumbers.size();i++)
        {
            if(favoriteRouteNumbers.get(i).equals(routeNumber)&&
                    favoriteRouteDirections.get(i).equals(routeDiretion))
                return true;
        }
        return false;
    }
    public void addFavoriteFindStations(String startStationName,String finishStationName)
    {
        favoriteStartStationNames.add(startStationName);
        favoriteFinishStationNames.add(finishStationName);
    }

    public void deleteFavoriteFindStations(String startStationName, String finishStationName) {
        for (int i = favoriteStartStationNames.size() - 1; i >= 0; i--) {
            if (favoriteStartStationNames.get(i).equals(startStationName) &&
                    favoriteFinishStationNames.get(i).equals(finishStationName)) {
                favoriteStartStationNames.remove(i);
                favoriteFinishStationNames.remove(i);
            }
        }
    }

    public Boolean isFavoriteFindStations(String startStationName, String finishStationName)
    {
        for(int i=0;i<favoriteStartStationNames.size();i++)
        {
            if(favoriteStartStationNames.get(i).equals(startStationName)&&
                    favoriteFinishStationNames.get(i).equals(finishStationName))
                return true;
        }
        return false;
    }
    public ArrayList<String> getFavoriteRouteNumbers() {
        return favoriteRouteNumbers;
    }

    public ArrayList<String> getFavoriteRouteDirections() {
        return favoriteRouteDirections;
    }

    public ArrayList<String> getFavoriteStationNames() {
        return favoriteStationNames;
    }

    public ArrayList<Integer> getFavoriteStationNumbers() {
        return favoriteStationNumbers;
    }

    public ArrayList<String> getFavoriteStartStationNames() {
        return favoriteStartStationNames;
    }

    public ArrayList<String> getFavoriteFinishStationNames() {
        return favoriteFinishStationNames;
    }
}
