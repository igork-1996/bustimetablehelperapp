package com.igorkondrashuk.bustimetablehelper;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.igorkondrashuk.bustimetablehelper.routemanager.Trip;

import java.util.ArrayList;

/**
 * Класс адаптер для отображения одной поездки на автобусе
 */
public class TripAdapter extends BaseAdapter
{
    Activity activity;
    ArrayList<Trip> arrayListRouteAnswer;
    TripAdapter(Activity activity, ArrayList<Trip> arrayListRouteAnswer)
    {
        this.activity=activity;
        this.arrayListRouteAnswer=arrayListRouteAnswer;
    }

    @Override
    public int getCount() {
        return arrayListRouteAnswer.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListRouteAnswer.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView=activity.getLayoutInflater().inflate(R.layout.trip,parent,false);
        TextView textViewRouteNumber=(TextView)convertView.findViewById(R.id.textViewRouteNumber);
        textViewRouteNumber.setText(arrayListRouteAnswer.get(position).getNumberRoute());
        TextView textViewRoueDirection=(TextView)convertView.findViewById(R.id.textViewRoueDirection);
        textViewRoueDirection.setText(arrayListRouteAnswer.get(position).getDirectionRoute());
        TextView textViewWeekDay=(TextView)convertView.findViewById(R.id.textViewWeekDay);
        switch (arrayListRouteAnswer.get(position).getDayRoute())
        {
            case "Р":
                textViewWeekDay.setText("Рабочие дни");
                break;
            case "В":
                textViewWeekDay.setText("Выходные дни");
                break;
            case "Р,В":
                textViewWeekDay.setText("Ежедневно");
                break;
            default:
                textViewWeekDay.setText(arrayListRouteAnswer.get(position).getDayRoute());
                break;
        }
        TextView textViewStartTime=(TextView)convertView.findViewById(R.id.textViewStartTime);
        textViewStartTime.setText(arrayListRouteAnswer.get(position).getTimeStart());
        TextView textViewStopTime=(TextView)convertView.findViewById(R.id.textViewStopTime);
        textViewStopTime.setText(arrayListRouteAnswer.get(position).getTimeStop());
        TextView textViewStartStation=(TextView)convertView.findViewById(R.id.textViewStartStation);
        textViewStartStation.setText(arrayListRouteAnswer.get(position).getNameStartStation());
        TextView textViewStopStation=(TextView)convertView.findViewById(R.id.textViewStopStation);
        textViewStopStation.setText(arrayListRouteAnswer.get(position).getNameFinishStation());
        return convertView;
    }
}
