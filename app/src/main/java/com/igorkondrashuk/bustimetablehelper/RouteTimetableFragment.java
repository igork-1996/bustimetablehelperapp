package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteTimetable;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

/**
 * Фрагмент для отображения расписания маршрута
 */
public class RouteTimetableFragment extends Fragment {
    RouteTimetable routeTimetable;
    View rootview;
    CardListView cardView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.route_timetable_fragment,container,false);
        if(routeTimetable!=null) {
            ArrayList<Card> cards = new ArrayList<>();
            CardlibDayComment timeCard=new CardlibDayComment(getActivity(),routeTimetable.getDayComment());
            cards.add(timeCard);
                for(int position=0;position<routeTimetable.getStations().size();position++) {
                    ArrayList<ArrayList<Integer>> toBold = new ArrayList<>();
                    ArrayList<ArrayList<Integer>> toRed = new ArrayList<>();
                    ArrayList<ArrayList<Integer>> toGreen = new ArrayList<>();
                    String strTemp="";
                    for (int i = 0; i < routeTimetable.getStations().get(position).size(); i++) {
                        ArrayList<Integer> tempToBold = new ArrayList<>();
                        if (strTemp.length() == 0)
                            tempToBold.add(strTemp.length());
                        else
                            tempToBold.add(strTemp.length() - 1);
                        switch (routeTimetable.getStations().get(position).get(i).getDayRoute()) {
                            case "Р,В":
                                strTemp += "Ежедневно\n";
                                break;
                            default:
                                strTemp += routeTimetable.getStations().get(position).get(i).getDayRoute() + "\n";
                                break;
                            case "Р":
                                strTemp += "Рабочие\n";
                                break;
                            case "В":
                                strTemp += "Выходные\n";
                                break;

                        }
                        tempToBold.add(strTemp.length() - 1);
                        toBold.add(tempToBold);
                        for (int j = 0; j < routeTimetable.getStations().get(position).get(i).getTimesRoute().size(); j++) {
                            ArrayList<Integer> tempToColor = new ArrayList<>();
                            switch(routeTimetable.getStations().get(position).get(i).getColorsTimesRoute().get(j))
                            {
                                case 0:
                                    //Оставляем черным
                                    strTemp+=routeTimetable.getStations().get(position).get(i).getTimesRoute().get(j);
                                    break;
                                case 1:
                                    //Красный цвет

                                    tempToColor.add(strTemp.length());
                                    strTemp+=routeTimetable.getStations().get(position).get(i).getTimesRoute().get(j);
                                    tempToColor.add(strTemp.length());
                                    toRed.add(tempToColor);
                                    break;
                                case 2:
                                    //Зеленый цвет
                                    tempToColor.add(strTemp.length());
                                    strTemp+=routeTimetable.getStations().get(position).get(i).getTimesRoute().get(j);
                                    tempToColor.add(strTemp.length());
                                    toGreen.add(tempToColor);
                                    break;
                                case 3:
                                    //Зеленый цвет жирным
                                    tempToColor.add(strTemp.length());
                                    strTemp+=routeTimetable.getStations().get(position).get(i).getTimesRoute().get(j);
                                    tempToColor.add(strTemp.length());
                                    toGreen.add(tempToColor);
                                    toBold.add(tempToColor);
                                    break;
                            }
                            strTemp+=" ";
                        }
                        if (i != routeTimetable.getStations().get(position).size() - 1) strTemp += "\n";
                    }

                SpannableStringBuilder sb = new SpannableStringBuilder(strTemp);
                for (int i = 0; i < toRed.size(); i++) {
                    ForegroundColorSpan style = new ForegroundColorSpan(Color.rgb(183, 28, 28));
                    sb.setSpan(style, toRed.get(i).get(0), toRed.get(i).get(1), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                }
                for (int i = 0; i < toGreen.size(); i++) {
                    ForegroundColorSpan style = new ForegroundColorSpan(Color.rgb(46, 125, 50));
                    sb.setSpan(style, toGreen.get(i).get(0), toGreen.get(i).get(1), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                }
                for (int i = 0; i < toBold.size(); i++) {
                    StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                    sb.setSpan(bss, toBold.get(i).get(0), toBold.get(i).get(1), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                }
                    CardlibRouteTimatable card;
                    if(routeTimetable.getStations().get(position).get(0).getStationNumber()>0)
                card = new CardlibRouteTimatable(getActivity(), routeTimetable.getStations().get(position).get(0).getStationName() + " ("+routeTimetable.getStations().get(position).get(0).getStationNumber()+")", sb);
                    else
                        card = new CardlibRouteTimatable(getActivity(), routeTimetable.getStations().get(position).get(0).getStationName() + " (-" +
                                ")", sb);

                    cards.add(card);
            }
            CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(getActivity(), cards);
            cardView = (CardListView) rootview.findViewById(R.id.cardlib_list_route_timetable);

            cardView.setAdapter(mCardArrayAdapter);
            final FloatingActionButton fab = (FloatingActionButton) rootview.findViewById(R.id.fab);
            fab.setColorNormalResId(R.color.primary_dark);
            fab.setShadow(false);
            final SharedPreferences sharedPref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
            String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper", "null");
            final Gson parser=new Gson();
            //Загрузка класса из SharedPreferences
            final SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper,new TypeToken<SharedPreferencesHelper>(){}.getType());
            //Если выбранного нет, то устанавливаем пустую звезду
            if (sharedPreferencesHelper.isFavoriteRoute(routeTimetable.getNumber(), routeTimetable.getDirection())) {
                Drawable tempImage = getResources().getDrawable(R.drawable.icon_full_star);
                fab.setImageDrawable(tempImage);
            }
            else
            {
                Drawable tempImage = getResources().getDrawable(R.drawable.ic_border_star);
                fab.setImageDrawable(tempImage);
            }

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sharedPreferencesHelper.isFavoriteRoute(routeTimetable.getNumber(), routeTimetable.getDirection())) {
                        sharedPreferencesHelper.deleteFavoriteRoute(routeTimetable.getNumber(), routeTimetable.getDirection());
                        Drawable tempImage = getResources().getDrawable(R.drawable.ic_border_star);
                        fab.setImageDrawable(tempImage);
                    }
                    else
                    {
                        sharedPreferencesHelper.addFavoriteRoute(routeTimetable.getNumber(), routeTimetable.getDirection());
                        Drawable tempImage = getResources().getDrawable(R.drawable.icon_full_star);
                        fab.setImageDrawable(tempImage);
                    }
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                    editor.apply();
                }

            });

            fab.attachToListView(cardView);
        }
        return rootview;
    }
    public void setRouteTimetable(RouteTimetable routeTimetable)
    {
        this.routeTimetable=routeTimetable;
    }
}
