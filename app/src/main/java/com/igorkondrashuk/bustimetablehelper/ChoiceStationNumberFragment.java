package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;



import java.util.ArrayList;

/**
 * Фрагмент для выбора номера остановки
 */
public class ChoiceStationNumberFragment extends Fragment{
    View rootview;
    //Номера остановок
    ArrayList<Integer> numbersString;
    //Названия остановок, в направлении которых идут автобусы
    ArrayList<ArrayList<String>> nextStations;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.choice_station_number_fragment,container,false);
        ArrayList<String> nextStationsForTextView=new ArrayList<>();
        if(numbersString!=null&&nextStations!=null) {
            //Формируем текст для списка
            for (int i = 0; i < nextStations.size(); i++) {
                if (nextStations.get(i).size() == 1) {
                    nextStationsForTextView.add("В сторону остановки: " + nextStations.get(i).get(0) + "" + " (номер " + numbersString.get(i) + ")");
                } else {
                    String tmpString =  "В сторону остановок:";
                    for (int j = 0; j < nextStations.get(i).size(); j++) {
                        if (j != 0)
                            tmpString += ", ";
                        else tmpString += " ";
                        tmpString += nextStations.get(i).get(j);
                    }
                    tmpString += " (номер " + numbersString.get(i)+")";
                    nextStationsForTextView.add(tmpString);
                }
            }
            //Устанавливаем текст в стандартный адаптер
            ArrayAdapter<String> adapter = new ArrayAdapter<>(rootview.getContext(), android.R.layout.simple_list_item_1, nextStationsForTextView);
            ListView tmp = (ListView) rootview.findViewById(R.id.list_stations);
            tmp.setAdapter(adapter);
            //Установка слушателя для нажатия на элемент списка
            someEventListener = (onSomeEventListener) getActivity();
            final ListView tmpListView = (ListView)rootview.findViewById(R.id.list_stations);
            tmpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Integer strNumber = numbersString.get(i);
                    someEventListener.someEvent("1.1", strNumber.toString());
                }
            });
        }
        return rootview;
    }
    //Метод устанавливает список остановок
    public void setNumbers(ArrayList<Integer> stations,ArrayList<ArrayList<String>> nextStations)
    {
        this.numbersString=stations;
        this.nextStations=nextStations;
    }

    public interface onSomeEventListener {
        void someEvent(String tagFragment,String stringSelected);
    }
    onSomeEventListener someEventListener;
}