package com.igorkondrashuk.bustimetablehelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Адаптер для отображения избранных остановок
 */

public class SelectedStationListAdapter extends BaseAdapter {
    ArrayList<String> stationName;
    ArrayList<Integer> stationNumber;
    ArrayList<String> text;
    Activity context;
    final SharedPreferences sharedPref;
    public SelectedStationListAdapter(Activity context, ArrayList<String> stationName, ArrayList<Integer> stationNumber,ArrayList<String> text) {
        this.text=text;
        this.stationName = stationName;
        this.stationNumber = stationNumber;
        this.context=context;
        sharedPref= context.getSharedPreferences("pref", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return stationName.size();
    }

    @Override
    public Object getItem(int position) {
        return stationName.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if(convertView==null)
            convertView=context.getLayoutInflater().inflate(R.layout.selected_station, parent, false);
        SpannableStringBuilder sb = new SpannableStringBuilder(stationName.get(position)+"\n"+text.get(position));
        StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(bss, 0, stationName.get(position).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        TextView textViewStation=(TextView)convertView.findViewById(R.id.textViewNameStation);
        textViewStation.setText(sb);
        ImageButton imageButton =(ImageButton)convertView.findViewById(R.id.imageButtonStarRoute);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper", "null");
                Gson parser=new Gson();
                //Загрузка класса из SharedPreferences
                SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
                }.getType());
                    if(sharedPreferencesHelper.isFavoriteStation(
                            stationName.get(position),
                            stationNumber.get(position)))
                    {
                        v.setBackgroundDrawable(parent.getResources().getDrawable(R.drawable.ic_border_star_gold_50));
                        sharedPreferencesHelper.deleteFavoriteStation(stationName.get(position),stationNumber.get(position));
                    }
                    else
                    {
                        v.setBackgroundDrawable(parent.getResources().getDrawable(R.drawable.ic_full_star_gold_50));
                        sharedPreferencesHelper.addFavoriteStation(stationName.get(position),stationNumber.get(position));
                    }
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                editor.apply();
            }
        });
        return convertView;
    }
}