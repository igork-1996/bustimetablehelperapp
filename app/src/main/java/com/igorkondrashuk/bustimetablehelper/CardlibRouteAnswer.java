package com.igorkondrashuk.bustimetablehelper;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.igorkondrashuk.bustimetablehelper.routemanager.RouteAnswer;
import com.igorkondrashuk.bustimetablehelper.routemanager.Trip;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;
import it.gmariotti.cardslib.library.prototypes.LinearListView;

/**
 * Класс для отображения карточки с одним вариантом для поездки
 */
public class CardlibRouteAnswer extends CardWithList {
    Activity activity;
    RouteAnswer routeAnswer;
    public CardlibRouteAnswer(Activity activity,RouteAnswer routeAnswer)
    {
        super(activity.getApplicationContext(),R.layout.cardlib_card_with_list);
        this.activity=activity;
        this.routeAnswer=routeAnswer;
        onMapDrawListener=(onMapDrawListener)activity;
    }

    @Override
    protected CardHeader initCardHeader() {
        return null;
    }

    @Override
    protected void initCard() {
        setSwipeable(false);
    }

    @Override
    protected List<ListObject> initChildren() {
        List<ListObject> listObjects=new ArrayList<>();
        for(Trip trip:routeAnswer.getArrayListTrip())
        {
            listObjects.add(new TripObject(this,trip));
        }
        return listObjects;
    }

    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent) {
        TripObject tripObject=(TripObject)object;
        TextView textViewRouteNumber=(TextView)convertView.findViewById(R.id.textViewRouteNumber);
        textViewRouteNumber.setText(tripObject.getTrip().getNumberRoute());
        TextView textViewRoueDirection=(TextView)convertView.findViewById(R.id.textViewRoueDirection);
        textViewRoueDirection.setText(tripObject.getTrip().getDirectionRoute());
        TextView textViewWeekDay=(TextView)convertView.findViewById(R.id.textViewWeekDay);
        switch (tripObject.getTrip().getDayRoute())
        {
            case "Р":
                textViewWeekDay.setText("Рабочие дни");
                break;
            case "В":
                textViewWeekDay.setText("Выходные дни");
                break;
            case "Р,В":
                textViewWeekDay.setText("Ежедневно");
                break;
            default:
                textViewWeekDay.setText(tripObject.getTrip().getDayRoute());
                break;
        }
        TextView textViewStartTime=(TextView)convertView.findViewById(R.id.textViewStartTime);
        textViewStartTime.setText(tripObject.getTrip().getTimeStart());
        TextView textViewStopTime=(TextView)convertView.findViewById(R.id.textViewStopTime);
        textViewStopTime.setText(tripObject.getTrip().getTimeStop());
        TextView textViewStartStation=(TextView)convertView.findViewById(R.id.textViewStartStation);
        textViewStartStation.setText(tripObject.getTrip().getNameStartStation());
        TextView textViewStopStation=(TextView)convertView.findViewById(R.id.textViewStopStation);
        textViewStopStation.setText(tripObject.getTrip().getNameFinishStation());
        return convertView;
    }

    @Override
    public int getChildLayoutId() {
        return R.layout.cardlib_child_layout_trip;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);
            TextView textViewTripTime = (TextView) view.findViewById(R.id.textViewTimeTrip);
            textViewTripTime.setText("Время в пути "+Integer.toString(routeAnswer.getTripTime())+" минут(ы)");
            ImageButton imageButton = (ImageButton) view.findViewById(R.id.imageButtonMap);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            onMapDrawListener.showRouteAnswerOnMap(routeAnswer);
                Toast.makeText(getContext(), "Click on map button", Toast.LENGTH_SHORT).show();
            }
        });
            //TripAdapter tripAdapter = new TripAdapter(activity, routeAnswer.getArrayListTrip());
//            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
//            for (int i = 0; i < tripAdapter.getCount(); i++) {
//                linearLayout.addView(tripAdapter.getView(i, null, null));
//            }
    }
    // -------------------------------------------------------------
    // Weather Object
    // -------------------------------------------------------------
    public interface onMapDrawListener {
        void showRouteAnswerOnMap(RouteAnswer routeAnswer);
    }
    onMapDrawListener onMapDrawListener;
    public class TripObject extends DefaultListObject {

        Trip trip;

        public TripObject(Card parentCard,Trip trip) {
            super(parentCard);
            init();
            this.trip=trip;
        }

        private void init() {
            setSwipeable(false);
            //OnClick Listener
            setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(LinearListView parent, View view, int position, ListObject object) {
                    Toast.makeText(getContext(), "Click on " + getObjectId(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public String getObjectId() {
            return trip.getTimeStop();
        }
        public Trip getTrip() {
            return trip;
        }
    }
}
