package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteManager;

import java.util.ArrayList;

/**
 * Фрагмент для отображения избранных остановок
 */
public class SelectedStationFragment extends Fragment{
    View rootview;
    //RouteManager необходим для поиска следующих по ходу движения остановок
    RouteManager routeManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.selected_station_fragment,container,false);
        if(routeManager !=null) {
            SharedPreferences sharedPref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
            String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper", "null");
            final Gson parser=new Gson();
            //Загрузка класса из SharedPreferences
            final SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
            }.getType());
            ArrayList<ArrayList<String>> nextStations=routeManager.getNextStationForNumbers(sharedPreferencesHelper.getFavoriteStationNumbers());
            //Массив со строками направлений
            ArrayList<String> stationDirection=new ArrayList<>();
            for (int i = 0; i < sharedPreferencesHelper.getFavoriteStationNames().size(); i++) {
                if (nextStations.get(i).size() == 1) {
                    stationDirection.add("в сторону остановки: " + nextStations.get(i).get(0) + "" + " (номер " + sharedPreferencesHelper.getFavoriteStationNumbers().get(i) + ")");
                } else {
                    String tmpString = "в сторону остановок:";
                    for (int j = 0; j < nextStations.get(i).size(); j++) {
                        if (j != 0)
                            tmpString += ", ";
                        else tmpString += " ";
                        tmpString += nextStations.get(i).get(j);
                    }
                    tmpString += " (номер " + sharedPreferencesHelper.getFavoriteStationNumbers().get(i) + ")";
                    stationDirection.add(tmpString);
                }
            }

            SelectedStationListAdapter selectedStationListAdapter= new SelectedStationListAdapter(getActivity(), sharedPreferencesHelper.getFavoriteStationNames(), sharedPreferencesHelper.getFavoriteStationNumbers(),stationDirection);
            ListView listView = (ListView) rootview.findViewById(R.id.list_stations);
            listView.setAdapter(selectedStationListAdapter);
            TextView textView=(TextView) rootview.findViewById(R.id.idempty);
            textView.setText("Нет избранных остановок");
            listView.setEmptyView(textView);
            someEventListener = (onSomeEventListener) getActivity();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String strNumber = sharedPreferencesHelper.getFavoriteStationNames().get(i) + "_" + sharedPreferencesHelper.getFavoriteStationNumbers().get(i);
                    someEventListener.someEvent("5.1", strNumber);
                }
            });
        }
        return rootview;
    }

    public void setRouteManager(RouteManager routeManager) {
        this.routeManager = routeManager;
    }

    public interface onSomeEventListener {
        void someEvent(String tagFragment, String stringSelected);
    }
    onSomeEventListener someEventListener;
}