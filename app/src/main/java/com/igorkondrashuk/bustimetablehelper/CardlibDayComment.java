package com.igorkondrashuk.bustimetablehelper;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Класс карточка для вывода информации о дне и отклонении от расписания
 */
public class CardlibDayComment extends Card {
    private String strDayComment;
    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);
        TextView txtViewDayComment=(TextView) view.findViewById(R.id.textDayComment);
        txtViewDayComment.setText(strDayComment);
        TextView txtViewInfoComment=(TextView) view.findViewById(R.id.textViewInfoComment);
        txtViewInfoComment.setText("Будьте внимательны, отклонение от расписания может быть от -3 до +5 минут.");
    }

    public CardlibDayComment(Context context, String strDayComment) {
        super(context,R.layout.cardlib_day_comment);
        this.strDayComment=strDayComment;
    }
}
