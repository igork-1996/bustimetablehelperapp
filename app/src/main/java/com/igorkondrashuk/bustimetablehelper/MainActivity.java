package com.igorkondrashuk.bustimetablehelper;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteAnswer;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteTimetable;
import com.igorkondrashuk.bustimetablehelper.routemanager.FindAnswer;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteManager;
import com.koushikdutta.ion.Ion;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity implements
        ChoiceStationFragment.onSomeEventListener,
        ChoiceStationNumberFragment.onSomeEventListener,
        ChoiceRouteNumberFragment.onSomeEventListener,
        ChoiceRouteDirectionFragment.onSomeEventListener,
        ChoiceFindStation.onSomeEventListener,
        ChoiceFindRouteDataFragment.onFindButtonEventListener,
        ChoiceFindRouteDataFragment.findDataListener,
        SelectedStationFragment.onSomeEventListener,
        SelectedRouteFragment.onSomeEventListener,
        CardlibRouteAnswer.onMapDrawListener
{
    AccountHeader accountHeader;
    String dateTimetable;
    String versionTimetable;

    @Override
    protected void onPause() {
        if (asyncLoadingData != null) {
            if (asyncLoadingData.getStatus() == AsyncTask.Status.RUNNING) {
                asyncLoadingData.cancel(true);
                super.onBackPressed();
            }
        }
        if (asyncCheckUpdate != null) {
            if (asyncCheckUpdate.getStatus() == AsyncTask.Status.RUNNING) {
                asyncCheckUpdate.cancel(true);
                super.onBackPressed();
            }
        }
        if (asyncFindRoutes != null) {
            if (asyncFindRoutes.getStatus() == AsyncTask.Status.RUNNING) {
                asyncFindRoutes.cancel(true);
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
                progressBar.setVisibility(View.INVISIBLE);
                ImageButton btn = (ImageButton) findViewById(R.id.imageButton);
                btn.setEnabled(true);
            }
        }
        super.onPause();
    }

    //Объект бокового списка
    public Drawer navigationDrawer;
    //Объект тулбара
    public Toolbar toolbar;
    //Объект RouteManager
    RouteManager routeManager;
    //Выбранная строка из расписания
    String selectedStationForTimetable = "";
    Boolean flagIsLockedDrawer = false;
    int defaultHeightToolbar;

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if (asyncLoadingData != null)
        {
            if (asyncLoadingData.getStatus() == AsyncTask.Status.RUNNING) {
                asyncLoadingData.cancel(true);
                super.onBackPressed();
                if (asyncCheckUpdate != null) {
                    if (asyncCheckUpdate.getStatus() == AsyncTask.Status.RUNNING) {
                        asyncCheckUpdate.cancel(true);
                        super.onBackPressed();
                    }
                }
            }
        }
        if (asyncFindRoutes != null)
        {
            if (asyncFindRoutes.getStatus() == AsyncTask.Status.RUNNING) {
                asyncFindRoutes.cancel(true);
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
                progressBar.setVisibility(View.INVISIBLE);
                ImageButton btn = (ImageButton) findViewById(R.id.imageButton);
                btn.setEnabled(true);
                return;
            }
        }


        if (fragmentManager.getBackStackEntryCount() != 0 && !navigationDrawer.isDrawerOpen()) {
            if (fragmentManager.getBackStackEntryCount() == 1) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
            }
            switch (fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName()) {
                case "1.1.1":
                    toolbar.setTitle("Выбор направления");
                    toolbar.setSubtitle(selectedStationForTimetable);
                    break;
                case "1.1":
                    toolbar.setTitle("Выбор остановки");
                    toolbar.setSubtitle("");
                    toolbar.getLayoutParams().height = defaultHeightToolbar;
                    break;
                case "1.2":
                    toolbar.setTitle("Выбор данных поиска");
                    toolbar.setSubtitle("");
                    break;
                case "2.1.1":
                    toolbar.setTitle("Выбор направления");
                    toolbar.setSubtitle("");
                    toolbar.getLayoutParams().height = defaultHeightToolbar;
                    break;
                case "2.1":
                    toolbar.setTitle("Выбор маршрута");
                    toolbar.setSubtitle("");
                    break;
                case "3.1":
                    toolbar.setTitle("Выбор данных поиска");
                    toolbar.setSubtitle("");
                    toolbar.getLayoutParams().height = defaultHeightToolbar;
                    break;
                case "5.1.1":
                    toolbar.setTitle("Избранные остановки");
                    toolbar.setSubtitle("");
                    toolbar.getLayoutParams().height = defaultHeightToolbar;
                    break;
                case "6.1.1":
                    toolbar.setTitle("Избранные маршруты");
                    toolbar.setSubtitle("");
                    toolbar.getLayoutParams().height = defaultHeightToolbar;
                    break;
                case "map":

                    break;
            }
            fragmentManager.popBackStackImmediate();
        } else if (navigationDrawer.isDrawerOpen() && fragmentManager.getBackStackEntryCount() != 0) {
            navigationDrawer.closeDrawer();
        } else if (!navigationDrawer.isDrawerOpen() && fragmentManager.getBackStackEntryCount() == 0) {
            navigationDrawer.openDrawer();
        } else {
            super.onBackPressed();
            if (asyncCheckUpdate != null) {
                if (asyncCheckUpdate.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCheckUpdate.cancel(true);
                    super.onBackPressed();
                }
            }
        }
    }
    //Фрагмент для выбора информации для поиска
    ChoiceFindRouteDataFragment choiceFindRouteDataFragment;
    //Поток загрузки данных
    AsyncLoadingData asyncLoadingData;
    //Поток проверки обновления
    AsyncCheckUpdate asyncCheckUpdate;
    //Метод инициализирует класс для работы с расписанием и запускает проверку на наличие обновления приложения
    private void readTimetable() throws PackageManager.NameNotFoundException {
        //Проверка на то первый ли запуск приложения после обновления, если да, то удаляем текущую базу данных из папки databases, что бы загрузить новую из assets
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
        int versionCodeSaved = sharedPref.getInt("version_code",0);
        PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        int versionCode = pInfo.versionCode;
        //если была установлена новая версия, то необходимо удалить старую версию файла sqlite(новая загрузится классом DataBaseHelper)
        if(versionCode!=versionCodeSaved)
        {
            File dbFile = getApplicationContext().getFileStreamPath("Routes.sqlite");
                dbFile.delete();
        }
        //Сохраняем отметку того, что база данных была обновлена
        SharedPreferences.Editor mEdit1 = sharedPref.edit();
        mEdit1.putInt("version_code", versionCode);
        mEdit1.apply();
//        //Инициализация класса для загрузки расписания
        DataBaseFileHelper dataBaseFileHelper=new DataBaseFileHelper(getApplicationContext());

//        DataBaseHelper dbOpenHelper = new DataBaseHelper(getApplicationContext());
        dateTimetable = dataBaseFileHelper.getDateTimetable();
        versionTimetable = dataBaseFileHelper.getVersionTimetable();
        //Запуск проверки на наличие обновлений
        asyncCheckUpdate=new AsyncCheckUpdate();
        asyncCheckUpdate.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //Получение класса для работы с расписанием
        try {
            routeManager =dataBaseFileHelper.getRouteManager();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonSharedPreferencesHelper=sharedPref.getString("shared_preferences_helper","null");
        if(jsonSharedPreferencesHelper.equals("null"))
        {
            SharedPreferencesHelper sharedPreferencesHelper=new SharedPreferencesHelper();
            Gson parser=new Gson();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
            editor.apply();
        }
        else
        {
            Gson parser=new Gson();
            //Загрузка класса из SharedPreferences
            SharedPreferencesHelper sharedPreferencesHelper=parser.fromJson(jsonSharedPreferencesHelper, new TypeToken<SharedPreferencesHelper>() {
            }.getType());
            //Проверка есть ли в расписании избранные остановки
            ArrayList<String> startStations=sharedPreferencesHelper.getFavoriteStartStationNames();
            ArrayList<String> finishStations=sharedPreferencesHelper.getFavoriteFinishStationNames();
            for(int i=0;i<startStations.size();i++)
            {
                if(!routeManager.getArrayListNameStations().contains(startStations.get(i))||
                        !routeManager.getArrayListNameStations().contains(finishStations.get(i))) {
                    sharedPreferencesHelper.deleteFavoriteFindStations(startStations.get(i), finishStations.get(i));
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                    editor.apply();
                }
            }
            //Проверка есть ли в расписании избранные остановки и номера остановок
            ArrayList<String> stationNames=sharedPreferencesHelper.getFavoriteStationNames();
            ArrayList<Integer> stationNumbers=sharedPreferencesHelper.getFavoriteStationNumbers();
            for(int i=0;i<stationNumbers.size();i++)
            {
                if(routeManager.getHashMapNumberNameStation().get(stationNumbers.get(i))==null||
                        !routeManager.getHashMapNumberNameStation().get(stationNumbers.get(i)).equals(stationNames.get(i))) {
                    sharedPreferencesHelper.deleteFavoriteStation(stationNames.get(i), stationNumbers.get(i));
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                    editor.apply();
                }
            }
            //Проверка есть ли в расписании избранные маршруты
            ArrayList<String> routeNumbers=sharedPreferencesHelper.getFavoriteRouteNumbers();
            ArrayList<String> routeDirections=sharedPreferencesHelper.getFavoriteRouteDirections();
            for(int i=0;i<routeNumbers.size();i++)
            {
                if(!routeManager.getAllNumberRoutes().contains(routeNumbers.get(i))||
                        !routeManager.getListRouteDirections(routeNumbers.get(i)).contains(routeDirections.get(i))) {
                    sharedPreferencesHelper.deleteFavoriteRoute(routeNumbers.get(i), routeDirections.get(i));
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("shared_preferences_helper", parser.toJson(sharedPreferencesHelper));
                    editor.apply();
                }
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void findDataChoiced(String tagFragment, String stringFirstStation, String secondStation, Calendar now) {
        this.firstStation = stringFirstStation;
        this.secondStation = secondStation;
        timeForFindAnswer=new GregorianCalendar(
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH),
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE)
        );
        asyncFindRoutes = new AsyncFindRoutes();
        asyncFindRoutes.execute();
    }

    @Override
    public void showRouteAnswerOnMap(RouteAnswer routeAnswer) {
        MapViewFragment fragment =(MapViewFragment) Fragment.instantiate(getApplicationContext(), MapViewFragment.class.getName());
        fragment.setRouteManager(routeManager);
        fragment.setRouteAnswer(routeAnswer);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment,"map");
        ft.addToBackStack("map");
        ft.commit();
    }

    //Класс для загрузки данных
    private class AsyncLoadingData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            // обновляем пользовательский интерфейс сразу после выполнения задачи
            super.onPreExecute();
            DrawerLayout drawerLayout = navigationDrawer.getDrawerLayout();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            flagIsLockedDrawer = true;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                readTimetable();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result)
        {
            createNavDrawer();
            DrawerLayout drawerLayout = navigationDrawer.getDrawerLayout();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            flagIsLockedDrawer = false;
        }
    }
    /**
     * Compares two version strings.
     *
     * Use this instead of String.compareTo() for a non-lexicographical
     * comparison that works for version strings. e.g. "1.10".compareTo("1.6").
     *
     * @note It does not work if "1.10" is supposed to be equal to "1.10.0".
     *
     * @param str1 a string of ordinal numbers separated by decimal points.
     * @param str2 a string of ordinal numbers separated by decimal points.
     * @return The result is a negative integer if str1 is _numerically_ less than str2.
     *         The result is a positive integer if str1 is _numerically_ greater than str2.
     *         The result is zero if the strings are _numerically_ equal.
     */
    public static int versionCompare(String str1, String str2) {
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        // compare first non-equal ordinal finishSrarionName
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return Integer.signum(diff);
        }
        // the strings are equal or one string is a substring of the other
        // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
        return Integer.signum(vals1.length - vals2.length);
    }
    private class AsyncCheckUpdate extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        JsonObject json;
        String temp;
        @Override
        protected Void doInBackground(Void... params) {
             temp="";
            try {
                json = Ion.with(getBaseContext())
                        .load("https://brestschedule.azurewebsites.net/api/getVersion").asJsonObject().get();
                temp=json.get("appVersion").getAsString()+" "+json.get("log").getAsString()+" "+json.get("schVersion").getAsString();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            //Дожидаемся выполнения потока загрузки данных, что бы вывести сообщение о налисии новой версии
            try {
                asyncLoadingData.get();
            } catch (InterruptedException | ExecutionException | CancellationException e) {
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(json!=null) {
                String text="";
                ArrayList<ArrayList<Integer>> toBold = new ArrayList<>();

                String appVersion=json.get("appVersion").getAsString();
                String versionName = BuildConfig.VERSION_NAME;
                String log=json.get("log").getAsString();
                if(!appVersion.equals(versionName))
                {
                    ArrayList<Integer> tempToBold = new ArrayList<>();
                    tempToBold.add(text.length());
                    text+="Новая версия приложения "+appVersion+"\n";
                    tempToBold.add(text.length() - 1);
                    toBold.add(tempToBold);
                    text+=log+"\n";
                }
                String appVersionOfSch=json.get("appVersionOfSch").getAsString();
                String strCurrentVersion = "";
                try {
                    PackageInfo pInfo=getPackageManager().getPackageInfo(getPackageName(),0);
                    strCurrentVersion=pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                String schVersion=json.get("schVersion").getAsString();
                if(!schVersion.equals(dateTimetable)) {
                    ArrayList<Integer> tempToBold = new ArrayList<>();
                    if (text.length() == 0)
                        tempToBold.add(text.length());
                    else
                        tempToBold.add(text.length() - 1);
                    text += "Обновлено расписание от " + schVersion;
                    tempToBold.add(text.length());
                    toBold.add(tempToBold);
                }
                SpannableStringBuilder sb = new SpannableStringBuilder(text);
                for (int i = 0; i < toBold.size(); i++) {
                    StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                    sb.setSpan(bss, toBold.get(i).get(0), toBold.get(i).get(1), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                }
                if(!text.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Доступно обновление")
                            .setMessage(sb)
                            .setCancelable(true)
                            .setNeutralButton("Позже",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })

                            .setNegativeButton("Перейти в Google Play", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                }
                            });
                    if(versionCompare(strCurrentVersion,appVersionOfSch)>=0)
                    {
                        builder.setPositiveButton("Загрузить файл расписания", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("Внимание")
                                        .setMessage("Будет загружена обновленная версия расписания, размером 2МБ. После загрузки приложение будет перезапущено.")
                                        .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                downloadFile("https://brestschedule.azurewebsites.net/api/getScheduleFile");
                                            }
                                        })
                                        .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // do nothing
                                            }
                                        })
                                        .show();
                            }
                    });
                    }
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        }
    }
    //функция загружает новый файл расписания по ссылке
    private void downloadFile(String url) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        new AsyncTask<String, Integer, File>() {
            private Exception m_error = null;
            @Override
            protected void onPreExecute() {
                progressDialog.setMessage("Загрузка файла...");
                progressDialog.setCancelable(false);
                progressDialog.setMax(100);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.show();
            }

            @Override
            protected File doInBackground(String... params) {
                URL url;
                HttpsURLConnection urlConnection;
                InputStream inputStream;
                int totalSize;
                int downloadedSize;
                byte[] buffer;
                int bufferLength;
                File file=null;
                File fileOut=null;
                FileOutputStream fos;
                try {
                    url = new URL(params[0]);
                    urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setConnectTimeout(5000);
                    urlConnection.connect();
                    file = new File("data/data/"+getApplicationContext().getPackageName()+"/databases/","NewRoutes.sqlite");
                    fos = new FileOutputStream(file);
                    inputStream = urlConnection.getInputStream();
                    totalSize = 2240000;//urlConnection.getContentLength();
                    downloadedSize = 0;
                    buffer = new byte[1024];
                    // читаем со входа и пишем в выход,
                    // с каждой итерацией публикуем прогресс
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        fos.write(buffer, 0, bufferLength);
                        downloadedSize += bufferLength;
                        publishProgress(downloadedSize, totalSize);
                    }
                    fos.close();
                    inputStream.close();
                    InputStream in = new FileInputStream(file);
                    fileOut = new File("data/data/"+getApplicationContext().getPackageName()+"/databases/","Routes.sqlite");
                    fileOut.delete();
                    OutputStream out = new FileOutputStream(fileOut);

                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    return file;
                } catch (IOException e) {
                    e.printStackTrace();
                    if(file!=null)
                        file.delete();
                    if(fileOut!=null)
                        fileOut.delete();
                    m_error = e;
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                progressDialog.setProgress((int) (((float)values[0] / (float) values[1]) * 100));
            }

            @Override
            protected void onPostExecute(File file) {
                // отображаем сообщение, если возникла ошибка
                if (m_error != null) {
                    m_error.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Проверьте интернет-соединение", Toast.LENGTH_LONG).show();
                    progressDialog.hide();
                    return;
                }
                // закрываем прогресс и удаляем временный файл
                progressDialog.hide();
                file.delete();
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        }.execute(url);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (routeManager == null)
            //Если процесс приложения был убит, то его необходимо перезапустить
            if (savedInstanceState != null) {
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }

        //Cчитываем данные из базы данных
        //routeManagerOld = DataHolder.routeManagerOld;

        //choiceFindRouteDataFragment.setArguments(routeManagerOld);
        //Выделение памяти для тулбара
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(-1);
        toolbar.setSubtitleTextColor(-1);
        defaultHeightToolbar = toolbar.getLayoutParams().height;
        //создание фона для navigation drawer
        accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .build();
        //Выделение памяти и настройка Navigation drawer
        navigationDrawer = new DrawerBuilder()
                .withAccountHeader(accountHeader)
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggleAnimated(true)
                .withActionBarDrawerToggle(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withIcon(R.drawable.icon_search).withName("Поиск подходящего маршрута").withIdentifier(3),
                        new PrimaryDrawerItem().withIcon(R.drawable.ic_station_24).withName("Расписание остановки").withIdentifier(1),
                        new PrimaryDrawerItem().withIcon(R.drawable.ic_station_starred_24).withName("Избранные остановки").withIdentifier(5),
                        new PrimaryDrawerItem().withIcon(R.drawable.ic_bus_24).withName("Расписание маршрута").withIdentifier(2),
                        new PrimaryDrawerItem().withIcon(R.drawable.ic_bus_starred_24).withName("Избранные маршруты").withIdentifier(6),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withIcon(R.drawable.ic_info_24).withName("Информация").withIdentifier(4)

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        FragmentManager fragmentManager = getFragmentManager();
                        int n = fragmentManager.getBackStackEntryCount();
                        for (int i = 0; i < n; ++i) {

                            fragmentManager
                                    .popBackStackImmediate();
                        }
                        if (asyncFindRoutes != null) {
                            if (asyncFindRoutes.getStatus() == AsyncTask.Status.RUNNING) {
                                asyncFindRoutes.cancel(true);
                                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
                                progressBar.setVisibility(View.INVISIBLE);
                                ImageButton btn = (ImageButton) findViewById(R.id.imageButton);
                                if(btn!=null)
                                btn.setEnabled(true);
                            }
                        }

                        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
                        switch (drawerItem.getIdentifier()) {
                            case 1:


                                ChoiceStationFragment choiceStationFragment = new ChoiceStationFragment();
                                choiceStationFragment.setStations(routeManager.getArrayListNameStations());
                                fragmentManager.beginTransaction()
                                        .replace(R.id.container, choiceStationFragment, "1")
                                        .commit();
                                toolbar.setTitle("Выбор остановки");
                                toolbar.setSubtitle("");
                                toolbar.getLayoutParams().height = defaultHeightToolbar;
                                break;
                            case 2:
                                ChoiceRouteNumberFragment choiceRouteNumberFragment = new ChoiceRouteNumberFragment();
                                choiceRouteNumberFragment.setNumbers(routeManager.getAllNumberRoutes());
                                fragmentManager.beginTransaction()
                                        .replace(R.id.container, choiceRouteNumberFragment, "2")
                                        .commit();

                                toolbar.setTitle("Выбор маршрута");
                                toolbar.setSubtitle("");
                                toolbar.getLayoutParams().height = defaultHeightToolbar;
                                break;
                            case 3:
                                choiceFindRouteDataFragment = new ChoiceFindRouteDataFragment();
                                fragmentManager.beginTransaction()
                                        .replace(R.id.container, choiceFindRouteDataFragment, "3")
                                        .commit();
                                toolbar.setTitle("Выбор данных поиска");
                                toolbar.setSubtitle("");
                                toolbar.getLayoutParams().height = defaultHeightToolbar;

                                break;
                            case 4:
                                InfoFragment infoFragment = new InfoFragment();
                                infoFragment.setInfo(versionTimetable, dateTimetable);
                                // Pushing MapView Fragment

                                toolbar.setTitle("Информация");
                                toolbar.setSubtitle("");
                                toolbar.getLayoutParams().height = defaultHeightToolbar;
                                break;
                            case 5:
                                SelectedStationFragment selectedStationFragment = new SelectedStationFragment();
                                selectedStationFragment.setRouteManager(routeManager);
                                fragmentManager.beginTransaction()
                                        .replace(R.id.container, selectedStationFragment, "5")
                                        .commit();
                                toolbar.setTitle("Избранные остановки");
                                toolbar.setSubtitle("");
                                toolbar.getLayoutParams().height = defaultHeightToolbar;
                                break;
                            case 6:

                                SelectedRouteFragment selectedRouteFragment = new SelectedRouteFragment();
                                fragmentManager.beginTransaction()
                                        .replace(R.id.container, selectedRouteFragment, "6")
                                        .commit();
                                toolbar.setTitle("Избранные маршруты");
                                toolbar.setSubtitle("");
                                toolbar.getLayoutParams().height = defaultHeightToolbar;
                                break;
                        }
                        navigationDrawer.closeDrawer();
                        return true;
                    }
                })
                .build();
        navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        FragmentManager fragmentManager = getFragmentManager();
        LoadingDataFragment loadingDataFragment = new LoadingDataFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.container, loadingDataFragment, "loading_data")
                .commit();

        //Считывание данных из базы
        toolbar.setTitle("");
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        if (fragmentManager.getBackStackEntryCount() != 0) {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }

        }
        asyncLoadingData = new AsyncLoadingData();
        asyncLoadingData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }
    public void createNavDrawer() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }

        });
        navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        //Устмновка выбранного первого элемента и вызов метода onclick
        navigationDrawer.setSelection(3, true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_empty, menu);
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            if (menuItem.getItemId() == R.id.home) {
                View view = MenuItemCompat.getActionView(menuItem);
                if (view != null) {
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                }
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    String selectedStation;
    String selectedRouteNumber;

    @Override
    public void someEvent(String tagFragment, String stringSelected) {
        FragmentManager fragmentManager = getFragmentManager();
        //Spinner spinner=(Spinner)findViewById(R.id.spinner);
        navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Calendar nowTime=new GregorianCalendar();
        switch (tagFragment) {
            case "1":
                selectedStation = stringSelected;
                ChoiceStationNumberFragment choiceStationNumberFragment = new ChoiceStationNumberFragment();
                ArrayList<ArrayList<String>> nextStations = routeManager.getNextStationForNumbers(routeManager.getArrayListStationNumbers(stringSelected));
                choiceStationNumberFragment.setNumbers(routeManager.getArrayListStationNumbers(stringSelected), nextStations);
                toolbar.setTitle("Выбор направления");
                toolbar.setSubtitle(stringSelected);
                selectedStationForTimetable = stringSelected;
                toolbar.getLayoutParams().height *= 1.3;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, choiceStationNumberFragment, "1.1")
                        .addToBackStack("1.1")
                        .commit();
                break;

            case "1.1":
                StationTimetableFragment stationTimetableFragment = new StationTimetableFragment();
                stationTimetableFragment.setStationTimetable(
                        routeManager.getStationTimetable(
                                selectedStation,
                                Integer.parseInt(stringSelected),
                                nowTime.get(Calendar.DAY_OF_MONTH),
                                nowTime.get(Calendar.MONTH)+1,
                                nowTime.get(Calendar.YEAR),
                                nowTime.get(Calendar.HOUR_OF_DAY),
                                nowTime.get(Calendar.MINUTE)));
                toolbar.setTitle("Расписание остановки" + "\n");
                toolbar.setSubtitle(selectedStation + " (номер " + stringSelected + ")");
                fragmentManager.beginTransaction()
                        .replace(R.id.container, stationTimetableFragment, "1.1.1")
                        .addToBackStack("1.1.1")
                        .commit();
                break;
            case "2":
                selectedRouteNumber = stringSelected;
                ChoiceRouteDirectionFragment choiceRouteDirectionFragment = new ChoiceRouteDirectionFragment();
                choiceRouteDirectionFragment.setDirections(routeManager.getListRouteDirections(selectedRouteNumber));
                toolbar.setTitle("Выбор направления");
                toolbar.getLayoutParams().height = defaultHeightToolbar;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, choiceRouteDirectionFragment, "2.1")
                        .addToBackStack("2.1")
                        .commit();
                break;
            case "1.2.1":

                if (fragmentManager.getBackStackEntryCount() != 0)
                    fragmentManager.popBackStack();
                ChoiceFindRouteDataFragment choiceFindRouteDataFragmentNew=(ChoiceFindRouteDataFragment)fragmentManager.findFragmentByTag("3");
                if (stringSelected != null)
                    choiceFindRouteDataFragmentNew.setFirstStation(stringSelected);
                toolbar.setTitle("Выбор данных поиска");
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
                break;
            case "1.3.1":
                if (fragmentManager.getBackStackEntryCount() != 0)
                    fragmentManager.popBackStack();
                if (stringSelected != null)
                    choiceFindRouteDataFragment.setSecondStaionStation(stringSelected);
                toolbar.setTitle("Выбор данных поиска");
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
                break;
            case "1.2":
                selectedRouteNumber = stringSelected;
                ChoiceFindStation choiceFindStation = new ChoiceFindStation();
                choiceFindStation.setStations(routeManager.getArrayListNameStations());
                if (stringSelected.equals("1")) {
                    toolbar.setTitle("Начальная остановка");
                    choiceFindStation.setStartOrStop(true);
                } else if (stringSelected.equals("2")) {
                    toolbar.setTitle("Конечная остановка");
                    choiceFindStation.setStartOrStop(false);
                }
                toolbar.getLayoutParams().height = defaultHeightToolbar;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, choiceFindStation, "1.2")
                        .addToBackStack("1.2")
                        .commit();
                break;
            case "2.1":

                RouteTimetableFragment routeTimetableFragment = new RouteTimetableFragment();
                RouteTimetable timetableRoute =
                        routeManager.getAllRouteTimetable(
                                selectedRouteNumber,
                                stringSelected,
                                nowTime.get(Calendar.YEAR),
                                nowTime.get(Calendar.MONTH)+1,
                                nowTime.get(Calendar.DAY_OF_MONTH),
                                nowTime.get(Calendar.HOUR_OF_DAY),
                                nowTime.get(Calendar.MINUTE));
                routeTimetableFragment.setRouteTimetable(timetableRoute);
                //routeTimetableFragment.setRoiteInfo(selectedRouteNumber, stringSelected);
                toolbar.setTitle("Расписание маршрута");
                toolbar.setSubtitle(selectedRouteNumber.replace("№", "") + " " + stringSelected);
                toolbar.getLayoutParams().height *= 1.3;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, routeTimetableFragment, "2.1.1")
                        .addToBackStack("2.1.1")
                        .commit();
                break;
            case "5.1":
                String[] s = stringSelected.split("_");
                StationTimetableFragment stationTimetableFragment2 = new StationTimetableFragment();
                stationTimetableFragment2.setStationTimetable(routeManager.getStationTimetable(
                        s[0],
                        Integer.parseInt(s[1]),
                        nowTime.get(Calendar.DAY_OF_MONTH),
                        nowTime.get(Calendar.MONTH) + 1,
                        nowTime.get(Calendar.YEAR),
                        nowTime.get(Calendar.HOUR_OF_DAY),
                        nowTime.get(Calendar.MINUTE)));
                //stationTimetableFragment2.setStationInfo(s[0], s[1]);
                toolbar.setTitle("Расписание остановки" + "\n");
                toolbar.setSubtitle(s[0] + " (номер " + s[1] + ")");
                toolbar.getLayoutParams().height *= 1.3;
                //toolbar.getLayoutParams().height*=1.3;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, stationTimetableFragment2, "5.1.1")
                        .addToBackStack("5.1.1")
                        .commit();
                break;
            case "6.1":
                String[] s1 = stringSelected.split("_");
                RouteTimetableFragment routeTimetableFragment1 = new RouteTimetableFragment();
                RouteTimetable timetableRoute1 = routeManager.getAllRouteTimetable(
                        s1[0],
                        s1[1],
                        nowTime.get(Calendar.DAY_OF_MONTH),
                        nowTime.get(Calendar.MONTH)+1,
                        nowTime.get(Calendar.YEAR),
                        nowTime.get(Calendar.HOUR_OF_DAY),
                        nowTime.get(Calendar.MINUTE));
                routeTimetableFragment1.setRouteTimetable(timetableRoute1);
                //routeTimetableFragment1.setRoiteInfo(s1[0], s1[1]);
                toolbar.setTitle("Расписание маршрута" + "\n");
                toolbar.setSubtitle(s1[0].replace("№", "") + " " + s1[1]);
                toolbar.getLayoutParams().height *= 1.3;
                //toolbar.getLayoutParams().height*=1.3;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, routeTimetableFragment1, "6.1.1")
                        .addToBackStack("6.1.1")
                        .commit();
                break;
        }


    }

    AsyncFindRoutes asyncFindRoutes;
    FindAnswer findAnswer;
    String firstStation;
    String secondStation;

//Класс для вычисления трудоемких операций
    Calendar timeForFindAnswer;
    private class AsyncFindRoutes extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
            //Включение видимости progressBar
            progressBar.setVisibility(View.VISIBLE);
            ImageButton btn = (ImageButton) findViewById(R.id.imageButton);
            //Отключаем возможность нажимать на кнопки при поиске
            btn.setEnabled(false);
            //TODO Отклчить все элементы фрагмента при поиске
        }

        @Override
        protected Void doInBackground(Void... params) {
            findAnswer=routeManager.findRoutes(firstStation,
                    secondStation,
                    timeForFindAnswer.get(Calendar.YEAR),
                    timeForFindAnswer.get(Calendar.MONTH)+1,
                    timeForFindAnswer.get(Calendar.DAY_OF_MONTH),
                    timeForFindAnswer.get(Calendar.HOUR_OF_DAY),
                    timeForFindAnswer.get(Calendar.MINUTE));
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setTitle("Подходящие маршруты");
            toolbar.setSubtitle("Между " + findAnswer.getStartStation()+ " и " + findAnswer.getFinishStation());
            toolbar.getLayoutParams().height *= 1.3;
            super.onPostExecute(result);
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
            progressBar.setVisibility(View.INVISIBLE);
            FindRouteFragment findRouteFragment = new FindRouteFragment();
            findRouteFragment.setFindInfo(findAnswer.getStartStation(), findAnswer.getFinishStation());
            findRouteFragment.setFindAnswer(findAnswer);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, findRouteFragment, "3.1")
                    .addToBackStack("3.1")
                    .commit();
        }
    }
}
