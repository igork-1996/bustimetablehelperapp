package com.igorkondrashuk.bustimetablehelper;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.igorkondrashuk.bustimetablehelper.routemanager.Route;
import com.igorkondrashuk.bustimetablehelper.routemanager.RouteManager;
import com.igorkondrashuk.bustimetablehelper.routemanager.TimeManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Класс работает с базой данных и позволяет получать данные из базы
 */
public class DataBaseFileHelper {
    Context context;
    String DATABASE_NAME;

    String dateTimetable;
    String versionTimetable;
    DataBaseFileHelper(Context context)
    {
        this.context=context;
        DATABASE_NAME="Routes.db";
        if(!databaseExists())
            createDatabase();
        File dbFile=context.getFileStreamPath(DATABASE_NAME);
        try {
            FileInputStream fis=new FileInputStream(dbFile);
            ObjectInputStream ois=new ObjectInputStream(fis);
            dateTimetable=(String)ois.readObject();
            versionTimetable=(String)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException | StreamCorruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Проверяет есть ли скопированная база данных в папке /databases/
    private boolean databaseExists() {
        File dbFile = context.getFileStreamPath(DATABASE_NAME);
        return dbFile.exists();
    }

    //Копирует базу данных из папки assets
    private void createDatabase() {
        String parentPath = context.getFileStreamPath(DATABASE_NAME).getParent();
        String path = context.getFileStreamPath(DATABASE_NAME).getPath();

        File file = new File(parentPath);
        if (!file.exists()) {
            if (!file.mkdir()) {
                return;
            }
        }

        InputStream is = null;
        OutputStream os = null;
        try {
            is = context.getAssets().open(DATABASE_NAME);
            os = new FileOutputStream(path);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public RouteManager getRouteManager() throws IOException {
        ArrayList<Route> allRoutes;
        ArrayList<Integer> allNumberStations;
        HashMap<Integer,String> gpsHashMap;
        HashMap<Integer,String> hashMapNumberNameStation;
        ArrayList<ArrayList<ArrayList<Integer>>> graph;
        TimeManager timeManager;
        String allRoutesStr=null;
        String allNumberStationsStr=null;
        String gpsHashMapStr=null;
        String hashMapNumberNameStationStr=null;
        String timeManagerStr=null;
        File dbFile=context.getFileStreamPath(DATABASE_NAME);
        try {
            FileInputStream fis=new FileInputStream(dbFile);
            ObjectInputStream ois=new ObjectInputStream(fis);
            //Считывание версии базы данных
            ois.readObject();
            ois.readObject();
            //Считывание остальной части базы данных
            allRoutesStr=(String)ois.readObject();
            allNumberStationsStr=(String)ois.readObject();
            gpsHashMapStr=(String)ois.readObject();
            hashMapNumberNameStationStr=(String)ois.readObject();
            timeManagerStr=(String)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException | StreamCorruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Gson gson=new Gson();
        allRoutes=gson.fromJson(allRoutesStr,new TypeToken<ArrayList<Route>>(){}.getType());
        allNumberStations=gson.fromJson(allNumberStationsStr,new TypeToken<ArrayList<Integer>>(){}.getType());
        gpsHashMap=gson.fromJson(gpsHashMapStr,new TypeToken<HashMap<Integer,String>>(){}.getType());
        hashMapNumberNameStation=gson.fromJson(hashMapNumberNameStationStr,new TypeToken<HashMap<Integer,String>>(){}.getType());
        timeManager=gson.fromJson(timeManagerStr,new TypeToken<TimeManager>(){}.getType());
        graph = new ArrayList<>();
        //Определяем максимальный номер остановки и выделяем память для каждого номера
        int maxStation=0;
        for (int i = 0; i < allNumberStations.size(); i++)
        {
            if(maxStation<allNumberStations.get(i))
                maxStation=allNumberStations.get(i);
        }
        for (int i = 0; i < maxStation+1; i++)
        {
            ArrayList<ArrayList<Integer>> stations = new ArrayList<>();
            for (int j = 0; j < maxStation+1; j++)
            {
                stations.add(new ArrayList<Integer>());
            }
            graph.add(stations);
        }
        //Отмечаем связи остановок на графе
        for(int i = 0; i<allRoutes.size(); i++) {
            for (int m = 0; m < allRoutes.get(i).getStationsNumber().size(); m++)
                for (int n = m + 1; n < allRoutes.get(i).getStationsNumber().size(); n++) {
                    //Конечные остановки не обозначены в исходном раписании номером, поэтому для их обозначения используются отрицательные номера
                    int tempM;
                    if (allRoutes.get(i).getStationsNumber().get(m)<0 )
                        tempM=allRoutes.get(i).getStationsNumber().get(m)*-1;
                    else
                        tempM=allRoutes.get(i).getStationsNumber().get(m);
                    int tempN;
                    if(allRoutes.get(i).getStationsNumber().get(n)<0)
                        tempN=allRoutes.get(i).getStationsNumber().get(n)*-1;
                    else
                        tempN=allRoutes.get(i).getStationsNumber().get(n);
                        graph.get(tempM)
                                .get(tempN).add(i);
                }
        }
        return new RouteManager(allRoutes,allNumberStations,gpsHashMap,hashMapNumberNameStation,graph,timeManager);
    }

    public String getDateTimetable() {
        return dateTimetable;
    }

    public String getVersionTimetable() {
        return versionTimetable;
    }
}
